<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "�� ������� - ��� ��������� �������������� � ����� ������, ���: ����������, ��������, ��������� � ������������� IT-������ ������������ ���������������� � ������-���������");
$APPLICATION->SetPageProperty("keywords", "�� �������, IT-������, ������������ ����������������, ������-���������, ����������, ��������, ���������, �������������");
$APPLICATION->SetPageProperty("title", "�� ������� - ���� �������");
$APPLICATION->SetTitle("���� �������");
?><?if ($APPLICATION->GetCurDir()=="/team/"):?>
<div style="margin-left: 30px;">
</div>
 <?endif;?> <?$APPLICATION->IncludeComponent(
	"bitrix:catalog",
	"services",
	array(
	"ACTION_VARIABLE" => "action",	// �������� ����������, � ������� ���������� ��������
		"ADD_ELEMENT_CHAIN" => "N",	// �������� �������� �������� � ������� ���������
		"ADD_PICT_PROP" => "-",
		"ADD_PROPERTIES_TO_BASKET" => "Y",	// ��������� � ������� �������� ������� � �����������
		"ADD_SECTIONS_CHAIN" => "Y",	// �������� ������ � ������� ���������
		"AJAX_MODE" => "N",	// �������� ����� AJAX
		"AJAX_OPTION_ADDITIONAL" => "",	// �������������� �������������
		"AJAX_OPTION_HISTORY" => "N",	// �������� �������� ��������� ��������
		"AJAX_OPTION_JUMP" => "N",	// �������� ��������� � ������ ����������
		"AJAX_OPTION_STYLE" => "Y",	// �������� ��������� ������
		"BASKET_URL" => "/personal/basket.php",	// URL, ������� �� �������� � �������� ����������
		"CACHE_FILTER" => "N",	// ���������� ��� ������������� �������
		"CACHE_GROUPS" => "Y",	// ��������� ����� �������
		"CACHE_TIME" => "36000000",	// ����� ����������� (���.)
		"CACHE_TYPE" => "A",	// ��� �����������
		"COMPONENT_TEMPLATE" => "services",
		"DETAIL_ADD_DETAIL_TO_SLIDER" => "N",	// ��������� ��������� �������� � �������
		"DETAIL_BACKGROUND_IMAGE" => "-",	// ���������� ������� �������� ��� ������� �� ��������
		"DETAIL_BRAND_USE" => "N",	// ������������ ��������� "������"
		"DETAIL_BROWSER_TITLE" => "-",	// ���������� ��������� ���� �������� �� ��������
		"DETAIL_CHECK_SECTION_ID_VARIABLE" => "N",	// ������������ ��� ������ �� ����������, ���� �� ����� ������ ��������
		"DETAIL_DETAIL_PICTURE_MODE" => "IMG",	// ����� ������ ��������� ��������
		"DETAIL_DISPLAY_NAME" => "N",	// �������� �������� ��������
		"DETAIL_DISPLAY_PREVIEW_TEXT_MODE" => "E",	// ����� �������� ��� ������ �� ��������� ��������
		"DETAIL_META_DESCRIPTION" => "-",	// ���������� �������� �������� �� ��������
		"DETAIL_META_KEYWORDS" => "-",	// ���������� �������� ����� �������� �� ��������
		"DETAIL_PROPERTY_CODE" => array(	// ��������
			0 => "",
			1 => "",
		),
		"DETAIL_SET_CANONICAL_URL" => "N",	// ������������� ������������ URL
		"DETAIL_USE_COMMENTS" => "N",	// �������� ������ � ������
		"DETAIL_USE_VOTE_RATING" => "N",	// �������� ������� ������
		"DISPLAY_BOTTOM_PAGER" => "N",	// �������� ��� �������
		"DISPLAY_TOP_PAGER" => "N",	// �������� ��� �������
		"ELEMENT_SORT_FIELD" => "sort",	// �� ������ ���� ��������� ������ � �������
		"ELEMENT_SORT_FIELD2" => "id",	// ���� ��� ������ ���������� ������� � �������
		"ELEMENT_SORT_ORDER" => "asc",	// ������� ���������� ������� � �������
		"ELEMENT_SORT_ORDER2" => "desc",	// ������� ������ ���������� ������� � �������
		"FILTER_VIEW_MODE" => "VERTICAL",	// ��� ����������� ������ �������
		"IBLOCK_ID" => "9",	// ��������
		"IBLOCK_TYPE" => "services",	// ��� ���������
		"INCLUDE_SUBSECTIONS" => "N",	// ���������� �������� ����������� �������
		"LABEL_PROP" => "-",
		"LINE_ELEMENT_COUNT" => "3",	// ���������� ���������, ��������� � ����� ������ �������
		"LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",	// URL �� ��������, ��� ����� ������� ������ ��������� ���������
		"LINK_IBLOCK_ID" => "",	// ID ���������, �������� �������� ������� � ������� ���������
		"LINK_IBLOCK_TYPE" => "",	// ��� ���������, �������� �������� ������� � ������� ���������
		"LINK_PROPERTY_SID" => "",	// ��������, � ������� �������� �����
		"LIST_BROWSER_TITLE" => "-",	// ���������� ��������� ���� �������� �� �������� �������
		"LIST_META_DESCRIPTION" => "-",	// ���������� �������� �������� �� �������� �������
		"LIST_META_KEYWORDS" => "-",	// ���������� �������� ����� �������� �� �������� �������
		"LIST_PROPERTY_CODE" => array(	// ��������
			0 => "BUTTON",
			1 => "",
		),
		"MESSAGE_404" => "",	// ��������� ��� ������ (�� ��������� �� ����������)
		"MESS_BTN_ADD_TO_BASKET" => "� �������",	// ����� ������ "�������� � �������"
		"MESS_BTN_BUY" => "������",	// ����� ������ "������"
		"MESS_BTN_COMPARE" => "���������",	// ����� ������ "���������"
		"MESS_BTN_DETAIL" => "���������",	// ����� ������ "���������"
		"MESS_NOT_AVAILABLE" => "��� � �������",	// ��������� �� ���������� ������
		"PAGER_BASE_LINK_ENABLE" => "N",	// �������� ��������� ������
		"PAGER_DESC_NUMBERING" => "N",	// ������������ �������� ���������
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// ����� ����������� ������� ��� �������� ���������
		"PAGER_SHOW_ALL" => "N",	// ���������� ������ "���"
		"PAGER_SHOW_ALWAYS" => "N",	// �������� ������
		"PAGER_TEMPLATE" => ".default",	// ������ ������������ ���������
		"PAGER_TITLE" => "������",	// �������� ���������
		"PAGE_ELEMENT_COUNT" => "30",	// ���������� ��������� �� ��������
		"PARTIAL_PRODUCT_PROPERTIES" => "N",	// ��������� ��������� � ������� ������, � ������� ��������� �� ��� ��������������
		"PRICE_CODE" => "",	// ��� ����
		"PRICE_VAT_INCLUDE" => "Y",	// �������� ��� � ����
		"PRICE_VAT_SHOW_VALUE" => "N",	// ���������� �������� ���
		"PRODUCT_ID_VARIABLE" => "id",	// �������� ����������, � ������� ���������� ��� ������ ��� �������
		"PRODUCT_PROPERTIES" => "",	// �������������� ������, ����������� � �������
		"PRODUCT_PROPS_VARIABLE" => "prop",	// �������� ����������, � ������� ���������� �������������� ������
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",	// �������� ����������, � ������� ���������� ���������� ������
		"SECTIONS_SHOW_PARENT_NAME" => "N",	// ���������� �������� �������
		"SECTIONS_VIEW_MODE" => "LINE",	// ��� ������ �����������
		"SECTION_BACKGROUND_IMAGE" => "-",	// ���������� ������� �������� ��� ������� �� ��������
		"SECTION_COUNT_ELEMENTS" => "N",	// ���������� ���������� ��������� � �������
		"SECTION_ID_VARIABLE" => "SECTION_ID",	// �������� ����������, � ������� ���������� ��� ������
		"SECTION_TOP_DEPTH" => "2",	// ������������ ������������ ������� ��������
		"SEF_FOLDER" => "/team/",	// ������� ��� (������������ ����� �����)
		"SEF_MODE" => "Y",	// �������� ��������� ���
		"SEF_URL_TEMPLATES" => array(
			"sections" => "",
			"section" => "#SECTION_CODE#/",
			"element" => "#SECTION_CODE#/#ELEMENT_CODE#/",
			"compare" => "compare.php?action=#ACTION_CODE#",
			"smart_filter" => "#SECTION_CODE#/filter/#SMART_FILTER_PATH#/apply/",
		),
		"SET_LAST_MODIFIED" => "N",	// ������������� � ���������� ������ ����� ����������� ��������
		"SET_STATUS_404" => "N",	// ������������� ������ 404
		"SET_TITLE" => "Y",	// ������������� ��������� ��������
		"SHOW_404" => "N",	// ����� ����������� ��������
		"SHOW_DEACTIVATED" => "N",	// ���������� ���������������� ������
		"SHOW_PRICE_COUNT" => "1",	// �������� ���� ��� ����������
		"SHOW_TOP_ELEMENTS" => "N",	// �������� ��� ���������
		"TEMPLATE_THEME" => "blue",	// �������� ����
		"TOP_ELEMENT_COUNT" => "9",	// ���������� ��������� ���������
		"TOP_ELEMENT_SORT_FIELD" => "sort",	// �� ������ ���� ��������� ������ � �������
		"TOP_ELEMENT_SORT_FIELD2" => "id",	// ���� ��� ������ ���������� ������� � �������
		"TOP_ELEMENT_SORT_ORDER" => "asc",	// ������� ���������� ������� � �������
		"TOP_ELEMENT_SORT_ORDER2" => "desc",	// ������� ������ ���������� ������� � �������
		"TOP_LINE_ELEMENT_COUNT" => "3",	// ���������� ���������, ��������� � ����� ������ �������
		"TOP_PROPERTY_CODE" => array(	// ��������
			0 => "",
			1 => "",
		),
		"TOP_VIEW_MODE" => "SECTION",
		"USE_COMPARE" => "N",	// ��������� ��������� �������
		"USE_ELEMENT_COUNTER" => "N",	// ������������ ������� ����������
		"USE_FILTER" => "N",	// ���������� ������
		"USE_MAIN_ELEMENT_SECTION" => "N",	// ������������ �������� ������ ��� ������ ��������
		"USE_PRICE_COUNT" => "N",	// ������������ ����� ��� � �����������
		"USE_PRODUCT_QUANTITY" => "N",	// ��������� �������� ���������� ������
		"USE_STORE" => "N",	// ���������� ���� "���������� ������ �� ������"
		"VARIABLE_ALIASES" => array(
			"compare" => array(
				"ACTION_CODE" => "action",
			),
		)
	)
);?> <?if ($APPLICATION->GetCurDir()=="/team/"):?> <br>
 <br>
<div id="news" class="service-scroll">
</div>
<div style="margin-left: 30px; margin-top: -40px;">
	<h2>������� ��������</h2>
 <br>
	 <?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"for_team",
	array(
	"ACTIVE_DATE_FORMAT" => "j F Y",	// ������ ������ ����
		"ADD_SECTIONS_CHAIN" => "N",	// �������� ������ � ������� ���������
		"AJAX_MODE" => "N",	// �������� ����� AJAX
		"AJAX_OPTION_ADDITIONAL" => "",	// �������������� �������������
		"AJAX_OPTION_HISTORY" => "N",	// �������� �������� ��������� ��������
		"AJAX_OPTION_JUMP" => "N",	// �������� ��������� � ������ ����������
		"AJAX_OPTION_STYLE" => "Y",	// �������� ��������� ������
		"CACHE_FILTER" => "N",	// ���������� ��� ������������� �������
		"CACHE_GROUPS" => "Y",	// ��������� ����� �������
		"CACHE_TIME" => "36000000",	// ����� ����������� (���.)
		"CACHE_TYPE" => "A",	// ��� �����������
		"CHECK_DATES" => "Y",	// ���������� ������ �������� �� ������ ������ ��������
		"COMPONENT_TEMPLATE" => ".default",
		"DETAIL_URL" => "",	// URL �������� ���������� ��������� (�� ��������� - �� �������� ���������)
		"DISPLAY_BOTTOM_PAGER" => "N",	// �������� ��� �������
		"DISPLAY_DATE" => "Y",	// �������� ���� ��������
		"DISPLAY_NAME" => "N",	// �������� �������� ��������
		"DISPLAY_PICTURE" => "N",	// �������� ����������� ��� ������
		"DISPLAY_PREVIEW_TEXT" => "Y",	// �������� ����� ������
		"DISPLAY_TOP_PAGER" => "N",	// �������� ��� �������
		"FIELD_CODE" => array(	// ����
			0 => "",
			1 => "",
		),
		"FILTER_NAME" => "",	// ������
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",	// �������� ������, ���� ��� ���������� ��������
		"IBLOCK_ID" => "1",	// ��� ��������������� �����
		"IBLOCK_TYPE" => "news",	// ��� ��������������� ����� (������������ ������ ��� ��������)
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// �������� �������� � ������� ���������
		"INCLUDE_SUBSECTIONS" => "N",	// ���������� �������� ����������� �������
		"MESSAGE_404" => "",	// ��������� ��� ������ (�� ��������� �� ����������)
		"NEWS_COUNT" => "4",	// ���������� �������� �� ��������
		"PAGER_BASE_LINK_ENABLE" => "N",	// �������� ��������� ������
		"PAGER_DESC_NUMBERING" => "N",	// ������������ �������� ���������
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// ����� ����������� ������� ��� �������� ���������
		"PAGER_SHOW_ALL" => "N",	// ���������� ������ "���"
		"PAGER_SHOW_ALWAYS" => "N",	// �������� ������
		"PAGER_TEMPLATE" => ".default",	// ������ ������������ ���������
		"PAGER_TITLE" => "�������",	// �������� ���������
		"PARENT_SECTION" => "",	// ID �������
		"PARENT_SECTION_CODE" => "",	// ��� �������
		"PREVIEW_TRUNCATE_LEN" => "",	// ������������ ����� ������ ��� ������ (������ ��� ���� �����)
		"PROPERTY_CODE" => array(	// ��������
			0 => "",
			1 => "",
		),
		"SET_BROWSER_TITLE" => "N",	// ������������� ��������� ���� ��������
		"SET_LAST_MODIFIED" => "N",	// ������������� � ���������� ������ ����� ����������� ��������
		"SET_META_DESCRIPTION" => "Y",	// ������������� �������� ��������
		"SET_META_KEYWORDS" => "N",	// ������������� �������� ����� ��������
		"SET_STATUS_404" => "N",	// ������������� ������ 404
		"SET_TITLE" => "N",	// ������������� ��������� ��������
		"SHOW_404" => "N",	// ����� ����������� ��������
		"SORT_BY1" => "ACTIVE_FROM",	// ���� ��� ������ ���������� ��������
		"SORT_BY2" => "SORT",	// ���� ��� ������ ���������� ��������
		"SORT_ORDER1" => "DESC",	// ����������� ��� ������ ���������� ��������
		"SORT_ORDER2" => "ASC",	// ����������� ��� ������ ���������� ��������
	)
);?> <?/*$APPLICATION->IncludeComponent(
	"bitrix:news", 
	"news", 
	array(
		"DISPLAY_DATE" => "Y",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"USE_SHARE" => "N",
		"SEF_MODE" => "Y",
		"AJAX_MODE" => "N",
		"IBLOCK_TYPE" => "news",
		"IBLOCK_ID" => "1",
		"NEWS_COUNT" => "4",
		"USE_SEARCH" => "N",
		"USE_RSS" => "N",
		"USE_RATING" => "N",
		"USE_CATEGORIES" => "N",
		"USE_FILTER" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_ORDER1" => "DESC",
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
		"CHECK_DATES" => "Y",
		"PREVIEW_TRUNCATE_LEN" => "",
		"LIST_ACTIVE_DATE_FORMAT" => "j F Y",
		"LIST_FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"LIST_PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"DISPLAY_NAME" => "N",
		"META_KEYWORDS" => "-",
		"META_DESCRIPTION" => "-",
		"BROWSER_TITLE" => "-",
		"DETAIL_ACTIVE_DATE_FORMAT" => "j F Y",
		"DETAIL_FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"DETAIL_PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"DETAIL_DISPLAY_TOP_PAGER" => "N",
		"DETAIL_DISPLAY_BOTTOM_PAGER" => "N",
		"DETAIL_PAGER_TITLE" => "��������",
		"DETAIL_PAGER_TEMPLATE" => "",
		"DETAIL_PAGER_SHOW_ALL" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"ADD_ELEMENT_CHAIN" => "N",
		"USE_PERMISSIONS" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"PAGER_TEMPLATE" => ".default",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"PAGER_TITLE" => "�������",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"TAGS_CLOUD_ELEMENTS" => "150",
		"PERIOD_NEW_TAGS" => "",
		"DISPLAY_AS_RATING" => "rating",
		"FONT_MAX" => "50",
		"FONT_MIN" => "10",
		"COLOR_NEW" => "3E74E6",
		"COLOR_OLD" => "C0C0C0",
		"TAGS_CLOUD_WIDTH" => "100%",
		"AJAX_OPTION_ADDITIONAL" => "",
		"SEF_FOLDER" => "/news/",
		"COMPONENT_TEMPLATE" => "news",
		"SET_LAST_MODIFIED" => "N",
		"DETAIL_SET_CANONICAL_URL" => "N",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"SHOW_404" => "N",
		"MESSAGE_404" => "",
		"SEF_URL_TEMPLATES" => array(
			"news" => "",
			"section" => "",
			"detail" => "#ELEMENT_CODE#/",
		)
	),
	false
);*/?> <a href="/news/" style="margin-right: 20px;" class="dop_menu_services">��� �������</a>
</div>
 <br>
 <br>
<?endif;?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>