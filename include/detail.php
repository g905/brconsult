<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<script type="text/javascript" src="/bitrix/js/main/ajax.js"></script>

<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/lib/jquery-1.10.1.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$('#for_fanc_block').click(function() {
		$('#for_fanc_block').fadeOut(500);
		$('#for_fanc').fadeOut(500);
	});
	$('#for_fanc_close').click(function() {
		$('#for_fanc_block').fadeOut(500);
		$('#for_fanc').fadeOut(500);
	});
	
	var scrollTop = window.pageYOffset || document.documentElement.scrollTop;
	var wwidth = document.documentElement.clientWidth/2 - 415;
	scrollTop = scrollTop + 50;
	$('#for_fanc').css({"top":scrollTop+"px","left":wwidth+"px"});
	
});
</script>

<div id="for_fanc_close"></div>

<?
CModule::IncludeModule('iblock');
$arSelect = Array("ID", "NAME", "DETAIL_TEXT", "DETAIL_PICTURE");
$arFilter = Array("IBLOCK_ID"=>3, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "ID"=>$_GET["ELEMENT_ID"]);
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);
while($ob = $res->GetNextElement())
{
	$arFields = $ob->GetFields();
?>

<div id="popap_el">
	<div id="yakor"></div>
	<div id="img_popap_el">
		<p><img src="<?=CFile::GetPath($arFields["DETAIL_PICTURE"]);?>"></p>
	</div>
		<br />
	<div id="name_popap_el">
		<p><?=$arFields["NAME"]?></p>
	</div>
		<br />
	<div id="detal_popap_el">
		<div id="popap_el_top"></div>
			<p><?=$arFields["DETAIL_TEXT"];?></p>
		<div id="popap_el_bot"></div>
	</div>
		<br />
	<div class="reviews-read-all-button" id="popap_all">
		<p><a class="button-yellow" href="/reviews/">��� ������</a></p>
	</div>
</div>
<?
}
?> 

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");?>