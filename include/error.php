<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<script type="text/javascript" src="/bitrix/js/main/ajax.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		var error = $("#er").html();
		$("#er_popap").html(error);
	});
</script>

<div id="er_popap"></div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");?>