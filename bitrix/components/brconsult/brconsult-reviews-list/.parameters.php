<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

$arComponentParameters = array(
    "GROUPS"     => array(),
    "PARAMETERS" => array(
        "CONTENT_MANAGERS_GROUP_ID" => array(
            "PARENT"   => "BASE",
            "NAME"     => "ID ������ �������-����������",
            "TYPE"     => "STRING",
            "MULTIPLE" => "N",
            "DEFAULT"  => "5",
            "REFRESH"  => "Y",
        ),
    ),
);
