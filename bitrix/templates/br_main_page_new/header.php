<!DOCTYPE html>

<!--
  Copyright (c) ��� "�� �������" 2015
  http://www.brconsult.pro/

-->

<html>
    <head>
		<?$APPLICATION->ShowHead();?>
		<title><?$APPLICATION->ShowTitle();?></title>
		<link rel="icon" href="http://brconsult.pro/favicon.ico" type="image/x-icon">        
		<link rel="shortcut icon" href="<?=SITE_TEMPLATE_PATH?>/favicon.ico">
		<link href="http://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css">
		<script src="bitrix/js/br/rotate3d/utils.js"></script>
		<?/*<script src="bitrix/js/br/rotate3d/flip-card.js"></script>*/?>
		<link href="/assets/css/custom.css" rel="stylesheet" type="text/css">

		<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/lib/jquery-1.10.1.min.js"></script>
		<!--
		<script type="text/javascript" src="<?//=SITE_TEMPLATE_PATH?>/js/lib/jquery.mousewheel-3.0.6.pack.js"></script>
		<script type="text/javascript" src="<?//=SITE_TEMPLATE_PATH?>/js/source/jquery.fancybox.js?v=2.1.5"></script>
		<link rel="stylesheet" type="text/css" href="<?//=SITE_TEMPLATE_PATH?>/js/source/jquery.fancybox.css?v=2.1.5" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?//=SITE_TEMPLATE_PATH?>/js/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" />
		<script type="text/javascript" src="<?//=SITE_TEMPLATE_PATH?>/js/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
		<link rel="stylesheet" type="text/css" href="<?//=SITE_TEMPLATE_PATH?>/js/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />
		<script type="text/javascript" src="<?//=SITE_TEMPLATE_PATH?>/js/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
		<script type="text/javascript" src="<?//=SITE_TEMPLATE_PATH?>/js/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>
		-->
		<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/script.js"></script>
		<!--
		<script type="text/javascript" src="<?//=SITE_TEMPLATE_PATH?>/js/fancy/lib/jquery.mousewheel-3.0.6.pack.js"></script>
		<script type="text/javascript" src="<?//=SITE_TEMPLATE_PATH?>/js/fancy/source/jquery.fancybox.js?v=2.1.5"></script>
		<link rel="stylesheet" type="text/css" href="<?//=SITE_TEMPLATE_PATH?>/js/fancy/source/jquery.fancybox.css?v=2.1.5" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?//=SITE_TEMPLATE_PATH?>/js/fancy/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" />
		<script type="text/javascript" src="<?//=SITE_TEMPLATE_PATH?>/js/fancy/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
		<link rel="stylesheet" type="text/css" href="<?//=SITE_TEMPLATE_PATH?>/js/fancy/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />
		<script type="text/javascript" src="<?//=SITE_TEMPLATE_PATH?>/js/fancy/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
		<script type="text/javascript" src="<?//=SITE_TEMPLATE_PATH?>/js/fancy/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>
		-->
		<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/fancybox/jquery.fancybox-1.3.1.js"></script>
		<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/js/fancybox/jquery.fancybox-1.3.1.css" media="screen" />
		
		
		<link href="<?=SITE_TEMPLATE_PATH?>/js/owl-carousel/owl.carousel.css" rel="stylesheet">
		<link href="<?=SITE_TEMPLATE_PATH?>/js/owl-carousel/owl.theme.css" rel="stylesheet">
		<script src="<?=SITE_TEMPLATE_PATH?>/js/owl-carousel/owl.carousel.js"></script>

	

		<!-- Yandex.Metrika counter -->
		<script type="text/javascript">
			(function (d, w, c) {
				(w[c] = w[c] || []).push(function() {
					try {
						w.yaCounter36646360 = new Ya.Metrika({
							id:36646360,
							clickmap:true,
							trackLinks:true,
							accurateTrackBounce:true,
							webvisor:true,
							trackHash:true
						});
					} catch(e) { }
				});
		
				var n = d.getElementsByTagName("script")[0],
					s = d.createElement("script"),
					f = function () { n.parentNode.insertBefore(s, n); };
				s.type = "text/javascript";
				s.async = true;
				s.src = "https://mc.yandex.ru/metrika/watch.js";
		
				if (w.opera == "[object Opera]") {
					d.addEventListener("DOMContentLoaded", f, false);
				} else { f(); }
			})(document, window, "yandex_metrika_callbacks");
		</script>
		<noscript><div><img src="https://mc.yandex.ru/watch/36646360" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
		<!-- /Yandex.Metrika counter -->
		 <?
	        CUtil::InitJSCore();
	        CJSCore::Init(array("fx"));
	        CJSCore::Init(array('ajax')); 
	        CJSCore::Init(array("popup")); 
	    ?>
	
		<script type="text/javascript">
			$(function() {
				$('a.popup').fancybox({
					'overlayShow': true,
					'padding': 30,
					'margin' : 20,
					'scrolling' : 'no',
					'titleShow': false,
					'type': 'ajax',
					'href': '/include/quest.php'
				});
			});

			$(function() {
				$('a.callback').fancybox({
					'overlayShow': true,
					/*'padding': 30,*/
					'margin' : 20,
					/*'scrolling' : 'no',*/
					'titleShow': false,
					'type': 'ajax',
					'href': '/include/callback.php'
				});
			});
			
			$(document).ready(function() {

			  var owl = $("#owl-demo");

			  owl.owlCarousel({
				  items : 3, //10 items above 1000px browser width
				  itemsDesktop : [1000,3], //5 items between 1000px and 901px
				  itemsDesktopSmall : [900,3], // betweem 900px and 601px
				  itemsTablet: [600,3], //2 items between 600 and 0
				  itemsMobile : 3 //false // itemsMobile disabled - inherit from itemsTablet option
			  });

			  // Custom Navigation Events
			  $("#owl-next-btn").click(function(){
				owl.trigger('owl.next');
			  })
			  $("#owl-prev-btn").click(function(){
				owl.trigger('owl.prev');
			  })
			  $("#owl-play-btn").click(function(){
				owl.trigger('owl.play',1000); //owl.play event accept autoPlay speed as second parameter
			  })
			  $("#owl-stop-btn").click(function(){
				owl.trigger('owl.stop');
			  })

    			var owl2 = $("#owl-demo2");

			  owl2.owlCarousel({
				  items : 3, //10 items above 1000px browser width
				  itemsDesktop : [1000,3], //5 items between 1000px and 901px
				  itemsDesktopSmall : [900,3], // betweem 900px and 601px
				  itemsTablet: [600,3], //2 items between 600 and 0
				  itemsMobile : 3 //false // itemsMobile disabled - inherit from itemsTablet option
			  });

			  // Custom Navigation Events
			  $("#owl-next-btn2").click(function(){
				owl2.trigger('owl.next');
			  })
			  $("#owl-prev-btn2").click(function(){
				owl2.trigger('owl.prev');
			  })
			  $("#owl-play-btn2").click(function(){
				owl2.trigger('owl.play',1000); //owl.play event accept autoPlay speed as second parameter
			  })
			  $("#owl-stop-btn2").click(function(){
				owl2.trigger('owl.stop');
			  })
			  
			  var owl3 = $("#owl-demo3");

			  owl3.owlCarousel({
				  items : 2, //10 items above 1000px browser width
				  itemsDesktop : [1000,2], //5 items between 1000px and 901px
				  itemsDesktopSmall : [900,2], // betweem 900px and 601px
				  itemsTablet: [600,2], //2 items between 600 and 0
				  itemsMobile : 2 //false // itemsMobile disabled - inherit from itemsTablet option
			  });

			  // Custom Navigation Events
			  $("#owl-next-btn3").click(function(){
				owl3.trigger('owl.next');
			  })
			  $("#owl-prev-btn3").click(function(){
				owl3.trigger('owl.prev');
			  })
			  $("#owl-play-btn3").click(function(){
				owl3.trigger('owl.play',1000); //owl.play event accept autoPlay speed as second parameter
			  })
			  $("#owl-stop-btn3").click(function(){
				owl3.trigger('owl.stop');
			  })
			  
			  
			$("#reviews_a").click(function (event) { 
				event.preventDefault();
				var id  = $(this).attr('href'),
					top = $(id).offset().top;
				$('body,html').animate({scrollTop: top}, 1000);
			});
			  
			 
			});
		</script>
		<script type="text/javascript" src="//vk.com/js/api/openapi.js?127"></script>

		<!-- VK Widget -->
		<div id="vk_community_messages"></div>
		<script type="text/javascript">
			VK.Widgets.CommunityMessages("vk_community_messages", 104103147, {shown: 0});
		</script>
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
		
		  ga('create', 'UA-84795134-1', 'auto');
		  ga('send', 'pageview');
		
		</script>


		<!--[if IE]>
			<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/ie.css" />
		<![endif]-->

		<!--[if lt IE 9]>
			<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/reject/reject.css" media="all" />
			<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/reject/reject.min.js"></script>
		<![endif]-->

		<script src="//vk.com/js/api/openapi.js?125" type="text/javascript"></script>
		<script type="text/javascript" src="//vk.com/js/api/openapi.js?137"></script>
    </head>
	<?$APPLICATION->ShowPanel();?>
    <body>
        <section class="main">
            <header>
                <div class="header-area">
                    <div class="wrap header-w">
                        <a href="/"><div class="logo"></div></a>
                        <?$APPLICATION->IncludeComponent(
							"bitrix:menu",
							"br_main_menu",
							Array(
								"ROOT_MENU_TYPE" => "top",
								"MAX_LEVEL" => "1",
								"CHILD_MENU_TYPE" => "left",
								"USE_EXT" => "N",
								"DELAY" => "N",
								"ALLOW_MULTI_SELECT" => "N",
								"MENU_CACHE_TYPE" => "N",
								"MENU_CACHE_TIME" => "3600",
								"MENU_CACHE_USE_GROUPS" => "Y",
								"MENU_CACHE_GET_VARS" => array()
							),
						false
						);?>

						<div class="socials socials-top">
							<a href="https://vk.com/brconsult" class="vk" target="_blank"></a>
							<a href="https://www.facebook.com/" class="fb" target="_blank"></a>
							<a href="https://www.instagram.com/" class="inst" target="_blank"></a>
						</div>


						<div class="phone-header-block">
	                        <div class="phone-header">
	                            <div class="phone-number"><a href="tel:+73412958811">+7(3412)95-88-11</a></div>
	                            <div class="phone-number" style="margin-top:-5px;"><a href="tel:88000000000">8-800-000-00-00</a></div>
	                        </div>
	                        <a href="#" class="callback">�������� ������</a>
	                        <!-- <div class="dropdown">
							  <div class="dropbtn"></div>
							  <div class="dropdown-content">
							    <a href="#" class="ru"></a>
							    <a href="#" class="en"></a>
							  </div>
							</div> -->
							<div id="langs">
								<div class="current_lang"></div>
								<div class="select_lagns">
									<div class="ru"></div>
									<div class="en"></div>
									<div class="rg"></div>
								</div>
							</div>
	                    </div>
                        
                    </div>
                </div>
            </header>
			<section class="content">
			