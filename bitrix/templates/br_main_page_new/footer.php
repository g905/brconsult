				</div>
			</section>
            <footer>
                <div class="section">
                    <div class="footer wrap">

                        <div class="foot1">
                            <a href="/" style="clear:both;display:block;"><div class="logo" style="display:block;margin-bottom:10px"></div></a>

                            <div class="socials socials-footer">
                                <a href="https://vk.com/brconsult" class="vk" target="_blank"></a>
                                <a href="https://www.facebook.com/" class="fb" target="_blank"></a>
                                <a href="https://www.instagram.com/" class="inst" target="_blank"></a>
                            </div>
                        </div>

                        <div class="foot2">
                            <?$APPLICATION->IncludeComponent(
                                "bitrix:menu",
                                "foot",
                                Array(
                                    "ROOT_MENU_TYPE" => "foot1",
                                    "MAX_LEVEL" => "1",
                                    "CHILD_MENU_TYPE" => "left",
                                    "USE_EXT" => "N",
                                    "DELAY" => "N",
                                    "ALLOW_MULTI_SELECT" => "N",
                                    "MENU_CACHE_TYPE" => "N",
                                    "MENU_CACHE_TIME" => "3600",
                                    "MENU_CACHE_USE_GROUPS" => "Y",
                                    "MENU_CACHE_GET_VARS" => array()
                                ),
                            false
                            );?>
                        </div>

                        <div class="foot3">
                            <?$APPLICATION->IncludeComponent(
                                "bitrix:menu",
                                "foot",
                                Array(
                                    "ROOT_MENU_TYPE" => "foot2",
                                    "MAX_LEVEL" => "1",
                                    "CHILD_MENU_TYPE" => "left",
                                    "USE_EXT" => "N",
                                    "DELAY" => "N",
                                    "ALLOW_MULTI_SELECT" => "N",
                                    "MENU_CACHE_TYPE" => "N",
                                    "MENU_CACHE_TIME" => "3600",
                                    "MENU_CACHE_USE_GROUPS" => "Y",
                                    "MENU_CACHE_GET_VARS" => array()
                                ),
                            false
                            );?>
                        </div>


                        <div class="foot4">
                            <ul class="foot-menu">
                                <li class="title"><a href="/contacts/">��������</a></li>
                            </ul>

                            <div class="foot-address">
                                <span>�����:</span> �. ������, ��. ������������, 71�
                            </div>

                            <div class="foot-phone"><a href="tel:+73412958811">+7(3412)95-88-11</a></div>
                            <div class="foot-phone" style="margin-top:5px;"><a href="tel:88000000000">8-800-000-00-00</a></div>
                            <a href="#" class="callback foot-callback">�������� ������</a>
                        </div>

                        <!--<div class="footer-questions">� ������, ���� � ��� �������� �������, ���������� �� ������� � ����� ���������� � ��� � �������� ������� �� ���. ��� ����� �������������� ��������������� ������:</div>
                        <div class="footer-question-button">
                            <a class="button-blue popup">������ ������</a>
                        </div>-->
                    </div>
                </div>

                <div class="footer-bottom">
                    <div class="footer wrap">
                        <a href="/include/�������� � ��������� ��������� ������.pdf" target="_blank">�������� ��������� ������������ ������</a>
                    </div>
                </div>
            </footer>
        </section>

		<div id="for_fanc_block"></div>
		<div id="for_fanc"></div>
		<div style="display: none;">
			<!-- begin of Top100 code -->

				<script id="top100Counter" type="text/javascript" src="http://counter.rambler.ru/top100.jcn?3141900"></script>
				<noscript>
					<a href="http://top100.rambler.ru/navi/3141900/">
					<img src="http://counter.rambler.ru/top100.cnt?3141900" alt="Rambler's Top100" border="0" />
					</a>

				</noscript>
			<!-- end of Top100 code -->

			<!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter33139458 = new Ya.Metrika({id:33139458,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    trackHash:true});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/33139458" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
			<!-- /Yandex.Metrika counter -->

			<!-- Rating@Mail.ru counter -->
<script type="text/javascript">
var _tmr = window._tmr || (window._tmr = []);
_tmr.push({id: "2697384", type: "pageView", start: (new Date()).getTime()});
(function (d, w, id) {
  if (d.getElementById(id)) return;
  var ts = d.createElement("script"); ts.type = "text/javascript"; ts.async = true; ts.id = id;
  ts.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//top-fwz1.mail.ru/js/code.js";
  var f = function () {var s = d.getElementsByTagName("script")[0]; s.parentNode.insertBefore(ts, s);};
  if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); }
})(document, window, "topmailru-code");
</script><noscript><div style="position:absolute;left:-10000px;">
<img src="//top-fwz1.mail.ru/counter?id=2697384;js=na" style="border:0;" height="1" width="1" alt="�������@Mail.ru" />
</div></noscript>
			<!-- //Rating@Mail.ru counter -->
		</div>

<script type="text/javascript">
    $('<style>'+
        '.scrollTop{ display:none; z-index:9999; position:fixed;'+
            'bottom:150px; right:24%; width:57px; height:57px;'+
            'background:url(/assets/img/up.png) 0 0 no-repeat; }'+'</style>').appendTo('body');
    var
    speed = 500,
    $scrollTop = $('<a href="#" class="scrollTop">').appendTo('body');      

    $scrollTop.click(function(e){
        e.preventDefault();

        $( 'html:not(:animated),body:not(:animated)' ).animate({ scrollTop: 0}, speed );
    });

    //���������
    function show_scrollTop(){
        ( $(window).scrollTop() > 300 ) ? $scrollTop.fadeIn(600) : $scrollTop.fadeOut(600);
    }
    $(window).scroll( function(){ show_scrollTop(); } );
    show_scrollTop();


    $(".select_lagns div").click(function(){
        ll = $(this).attr("class");
        $(".current_lang").attr("class","current_lang");
        $(".current_lang").addClass(ll);
        $(".select_lagns").hide();
    });

    $("#langs").hover(
        function(){
            $(".select_lagns").show();
        },
        function(){
            $(".select_lagns").hide();
        }
    );
</script>

    </body>
</html>