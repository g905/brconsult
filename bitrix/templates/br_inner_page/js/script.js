$(document).ready(function() {
	$('.fancybox').fancybox();
	
	$("a[rel=example_group]").fancybox({
		'transitionIn'		: 'none',
		'transitionOut'		: 'none',
		'titlePosition' 	: 'over',
		'titleFormat'		: function(title, currentArray, currentIndex, currentOpts) {
			return '<span id="fancybox-title-over">Изображение ' + (currentIndex + 1) + ' / ' + currentArray.length + (title.length ? ' <br /> ' + title : '') + '</span>';
		}
	});
	
	$('.fancybox-buttons').fancybox({
		openEffect  : 'none',
		closeEffect : 'none',

		prevEffect : 'none',
		nextEffect : 'none',

		closeBtn  : false,

		helpers : {
			title : {
				type : 'inside'
			},
			buttons	: {}
		},

		afterLoad : function() {
			this.title = 'Изображение ' + (this.index + 1) + ' из ' + this.group.length + (this.title ? ' <br /> ' + this.title : '');
		}
	});
	
	$('#br_present_name').each(function() {
		 var  default_value = $(this).val();
		 $(this).focus(function(){
			if($(this).val() == default_value) {
			   $(this).val('');
			}
		 });
		 $(this).blur(function(){
			if($(this).val() == '') {
			   $(this).val(default_value);
			}
		 });
	});
	
	$('#br_present_phone').each(function() {
		 var  default_value = $(this).val();
		 $(this).focus(function(){
			if($(this).val() == default_value) {
			   $(this).val('');
			}
		 });
		 $(this).blur(function(){
			if($(this).val() == '') {
			   $(this).val(default_value);
			}
		 });
	});
	
	$('#br_present_org').each(function() {
		 var  default_value = $(this).val();
		 $(this).focus(function(){
			if($(this).val() == default_value) {
			   $(this).val('');
			}
		 });
		 $(this).blur(function(){
			if($(this).val() == '') {
			   $(this).val(default_value);
			}
		 });
	});
});