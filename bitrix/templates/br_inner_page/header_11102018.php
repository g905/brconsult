<!DOCTYPE html>

<!--
  Copyright (c) ��� "�� �������" 2015
  http://www.brconsult.pro/

-->

<html>
    <head>
		<?$APPLICATION->ShowHead();?>
		<title><?$APPLICATION->ShowTitle();?></title>
		<link rel="icon" href="http://brconsult.pro/favicon.ico" type="image/x-icon">        
		<link rel="shortcut icon" href="<?=SITE_TEMPLATE_PATH?>/favicon.ico">
		<link href="http://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css">
		<script src="bitrix/js/br/rotate3d/utils.js"></script>
		<?/*<script src="bitrix/js/br/rotate3d/flip-card.js"></script>*/?>
		<link href="/assets/css/custom.css" rel="stylesheet" type="text/css">

		<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/lib/jquery-1.10.1.min.js"></script>
		<!--
		<script type="text/javascript" src="<?//=SITE_TEMPLATE_PATH?>/js/lib/jquery.mousewheel-3.0.6.pack.js"></script>
		<script type="text/javascript" src="<?//=SITE_TEMPLATE_PATH?>/js/source/jquery.fancybox.js?v=2.1.5"></script>
		<link rel="stylesheet" type="text/css" href="<?//=SITE_TEMPLATE_PATH?>/js/source/jquery.fancybox.css?v=2.1.5" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?//=SITE_TEMPLATE_PATH?>/js/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" />
		<script type="text/javascript" src="<?//=SITE_TEMPLATE_PATH?>/js/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
		<link rel="stylesheet" type="text/css" href="<?//=SITE_TEMPLATE_PATH?>/js/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />
		<script type="text/javascript" src="<?//=SITE_TEMPLATE_PATH?>/js/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
		<script type="text/javascript" src="<?//=SITE_TEMPLATE_PATH?>/js/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>
		-->
		<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/script.js"></script>
		<!--
		<script type="text/javascript" src="<?//=SITE_TEMPLATE_PATH?>/js/fancy/lib/jquery.mousewheel-3.0.6.pack.js"></script>
		<script type="text/javascript" src="<?//=SITE_TEMPLATE_PATH?>/js/fancy/source/jquery.fancybox.js?v=2.1.5"></script>
		<link rel="stylesheet" type="text/css" href="<?//=SITE_TEMPLATE_PATH?>/js/fancy/source/jquery.fancybox.css?v=2.1.5" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?//=SITE_TEMPLATE_PATH?>/js/fancy/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" />
		<script type="text/javascript" src="<?//=SITE_TEMPLATE_PATH?>/js/fancy/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
		<link rel="stylesheet" type="text/css" href="<?//=SITE_TEMPLATE_PATH?>/js/fancy/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />
		<script type="text/javascript" src="<?//=SITE_TEMPLATE_PATH?>/js/fancy/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
		<script type="text/javascript" src="<?//=SITE_TEMPLATE_PATH?>/js/fancy/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>
		-->
		<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/fancybox/jquery.fancybox-1.3.1.js"></script>
		<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/js/fancybox/jquery.fancybox-1.3.1.css" media="screen" />
		
		
		<link href="<?=SITE_TEMPLATE_PATH?>/js/owl-carousel/owl.carousel.css" rel="stylesheet">
		<link href="<?=SITE_TEMPLATE_PATH?>/js/owl-carousel/owl.theme.css" rel="stylesheet">
		<script src="<?=SITE_TEMPLATE_PATH?>/js/owl-carousel/owl.carousel.js"></script>

	

		<!-- Yandex.Metrika counter -->
		<script type="text/javascript">
			(function (d, w, c) {
				(w[c] = w[c] || []).push(function() {
					try {
						w.yaCounter36646360 = new Ya.Metrika({
							id:36646360,
							clickmap:true,
							trackLinks:true,
							accurateTrackBounce:true,
							webvisor:true,
							trackHash:true
						});
					} catch(e) { }
				});
		
				var n = d.getElementsByTagName("script")[0],
					s = d.createElement("script"),
					f = function () { n.parentNode.insertBefore(s, n); };
				s.type = "text/javascript";
				s.async = true;
				s.src = "https://mc.yandex.ru/metrika/watch.js";
		
				if (w.opera == "[object Opera]") {
					d.addEventListener("DOMContentLoaded", f, false);
				} else { f(); }
			})(document, window, "yandex_metrika_callbacks");
		</script>
		<noscript><div><img src="https://mc.yandex.ru/watch/36646360" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
		<!-- /Yandex.Metrika counter -->
		 <?
	        CUtil::InitJSCore();
	        CJSCore::Init(array("fx"));
	        CJSCore::Init(array('ajax')); 
	        CJSCore::Init(array("popup")); 
	    ?>
	
		<script type="text/javascript">
			$(function() {
				$('a.popup').fancybox({
					'overlayShow': true,
					'padding': 30,
					'margin' : 20,
					'scrolling' : 'no',
					'titleShow': false,
					'type': 'ajax',
					'href': '/include/quest.php'
				});
			});

			$(function() {
				$('a.callback').fancybox({
					'overlayShow': true,
					/*'padding': 30,*/
					'margin' : 20,
					/*'scrolling' : 'no',*/
					'titleShow': false,
					'type': 'ajax',
					'href': '/include/callback.php'
				});
			});
			
			$(document).ready(function() {

			  var owl = $("#owl-demo");

			  owl.owlCarousel({
				  items : 3, //10 items above 1000px browser width
				  itemsDesktop : [1000,3], //5 items between 1000px and 901px
				  itemsDesktopSmall : [900,3], // betweem 900px and 601px
				  itemsTablet: [600,3], //2 items between 600 and 0
				  itemsMobile : 3 //false // itemsMobile disabled - inherit from itemsTablet option
			  });

			  // Custom Navigation Events
			  $("#owl-next-btn").click(function(){
				owl.trigger('owl.next');
			  })
			  $("#owl-prev-btn").click(function(){
				owl.trigger('owl.prev');
			  })
			  $("#owl-play-btn").click(function(){
				owl.trigger('owl.play',1000); //owl.play event accept autoPlay speed as second parameter
			  })
			  $("#owl-stop-btn").click(function(){
				owl.trigger('owl.stop');
			  })

    			var owl2 = $("#owl-demo2");

			  owl2.owlCarousel({
				  items : 3, //10 items above 1000px browser width
				  itemsDesktop : [1000,3], //5 items between 1000px and 901px
				  itemsDesktopSmall : [900,3], // betweem 900px and 601px
				  itemsTablet: [600,3], //2 items between 600 and 0
				  itemsMobile : 3 //false // itemsMobile disabled - inherit from itemsTablet option
			  });

			  // Custom Navigation Events
			  $("#owl-next-btn2").click(function(){
				owl2.trigger('owl.next');
			  })
			  $("#owl-prev-btn2").click(function(){
				owl2.trigger('owl.prev');
			  })
			  $("#owl-play-btn2").click(function(){
				owl2.trigger('owl.play',1000); //owl.play event accept autoPlay speed as second parameter
			  })
			  $("#owl-stop-btn2").click(function(){
				owl2.trigger('owl.stop');
			  })
			  
			  var owl3 = $("#owl-demo3");

			  owl3.owlCarousel({
				  items : 2, //10 items above 1000px browser width
				  itemsDesktop : [1000,2], //5 items between 1000px and 901px
				  itemsDesktopSmall : [900,2], // betweem 900px and 601px
				  itemsTablet: [600,2], //2 items between 600 and 0
				  itemsMobile : 2 //false // itemsMobile disabled - inherit from itemsTablet option
			  });

			  // Custom Navigation Events
			  $("#owl-next-btn3").click(function(){
				owl3.trigger('owl.next');
			  })
			  $("#owl-prev-btn3").click(function(){
				owl3.trigger('owl.prev');
			  })
			  $("#owl-play-btn3").click(function(){
				owl3.trigger('owl.play',1000); //owl.play event accept autoPlay speed as second parameter
			  })
			  $("#owl-stop-btn3").click(function(){
				owl3.trigger('owl.stop');
			  })
			  
			  
			$("#reviews_a").click(function (event) { 
				event.preventDefault();
				var id  = $(this).attr('href'),
					top = $(id).offset().top;
				$('body,html').animate({scrollTop: top}, 1000);
			});
			  
			 
			});
		</script>
		<script type="text/javascript" src="//vk.com/js/api/openapi.js?127"></script>

		<!-- VK Widget -->
		<div id="vk_community_messages"></div>
		<script type="text/javascript">
			VK.Widgets.CommunityMessages("vk_community_messages", 104103147, {shown: 0});
		</script>
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
		
		  ga('create', 'UA-84795134-1', 'auto');
		  ga('send', 'pageview');
		
		</script>


		<!--[if IE]>
			<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/ie.css" />
		<![endif]-->

		<!--[if lt IE 9]>
			<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/reject/reject.css" media="all" />
			<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/reject/reject.min.js"></script>
		<![endif]-->

		<script src="//vk.com/js/api/openapi.js?125" type="text/javascript"></script>
		<script type="text/javascript" src="//vk.com/js/api/openapi.js?137"></script>
    </head>
	<?$APPLICATION->ShowPanel();?>
    <body>
        <section class="main">
            <header>
                <div class="header-area">
                    <div class="wrap header-w">
                        <a href="/"><div class="logo"></div></a>
                        <?$APPLICATION->IncludeComponent(
							"bitrix:menu",
							"br_main_menu",
							Array(
								"ROOT_MENU_TYPE" => "top",
								"MAX_LEVEL" => "1",
								"CHILD_MENU_TYPE" => "left",
								"USE_EXT" => "N",
								"DELAY" => "N",
								"ALLOW_MULTI_SELECT" => "N",
								"MENU_CACHE_TYPE" => "N",
								"MENU_CACHE_TIME" => "3600",
								"MENU_CACHE_USE_GROUPS" => "Y",
								"MENU_CACHE_GET_VARS" => array()
							),
						false
						);?>

						<div class="socials socials-top">
							<a href="https://vk.com/brconsult" class="vk" target="_blank"></a>
							<a href="https://www.facebook.com/" class="fb" target="_blank"></a>
							<a href="https://www.instagram.com/" class="inst" target="_blank"></a>
						</div>


						<div class="phone-header-block">
	                        <div class="phone-header">
	                            <div class="phone-number"><a href="tel:+73412958811">+7(3412)95-88-11</a></div>
	                            <div class="phone-number" style="margin-top:-5px;"><a href="tel:88000000000">8-800-000-00-00</a></div>
	                        </div>
	                        <a href="#" class="callback">�������� ������</a>
	                        <!-- <div class="dropdown">
							  <div class="dropbtn"></div>
							  <div class="dropdown-content">
							    <a href="#" class="ru"></a>
							    <a href="#" class="en"></a>
							  </div>
							</div> -->
							<div id="langs">
								<div class="current_lang"></div>
								<div class="select_lagns">
									<div class="ru"></div>
									<div class="en"></div>
									<div class="rg"></div>
								</div>
							</div>
	                    </div>
                        
                    </div>
                </div>
            </header>
			
	<section class="content2"></section>
	<section class="content">
		<div class="wrap">
			<div class="nav-side">
				<div class="nav-title">��������� �� �������</div>
				<div class="nav-border"></div>
				
				<nav>
<?if(CSite::InDir('/services/')):?>
<?$APPLICATION->IncludeComponent(
	"bitrix:catalog", 
	"for_menu", 
	array(
		"IBLOCK_TYPE" => "services",
		"IBLOCK_ID" => "7",
		"SEF_MODE" => "Y",
		"SEF_FOLDER" => "/services/",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"ADD_ELEMENT_CHAIN" => "N",
		"USE_ELEMENT_COUNTER" => "N",
		"TEMPLATE_THEME" => "blue",
		"ADD_PICT_PROP" => "-",
		"LABEL_PROP" => "-",
		"MESS_BTN_BUY" => "������",
		"MESS_BTN_ADD_TO_BASKET" => "� �������",
		"MESS_BTN_COMPARE" => "���������",
		"MESS_BTN_DETAIL" => "���������",
		"MESS_NOT_AVAILABLE" => "��� � �������",
		"DETAIL_USE_VOTE_RATING" => "N",
		"DETAIL_USE_COMMENTS" => "N",
		"DETAIL_BRAND_USE" => "N",
		"USE_FILTER" => "N",
		"ACTION_VARIABLE" => "action",
		"PRODUCT_ID_VARIABLE" => "id",
		"USE_COMPARE" => "N",
		"PRICE_CODE" => array(
		),
		"USE_PRICE_COUNT" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"PRICE_VAT_INCLUDE" => "Y",
		"PRICE_VAT_SHOW_VALUE" => "N",
		"BASKET_URL" => "/personal/basket.php",
		"USE_PRODUCT_QUANTITY" => "N",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRODUCT_PROPERTIES" => array(
		),
		"SHOW_TOP_ELEMENTS" => "N",
		"TOP_ELEMENT_COUNT" => "9",
		"TOP_LINE_ELEMENT_COUNT" => "3",
		"TOP_ELEMENT_SORT_FIELD" => "sort",
		"TOP_ELEMENT_SORT_ORDER" => "asc",
		"TOP_ELEMENT_SORT_FIELD2" => "id",
		"TOP_ELEMENT_SORT_ORDER2" => "desc",
		"TOP_PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"TOP_VIEW_MODE" => "SECTION",
		"SECTION_COUNT_ELEMENTS" => "N",
		"SECTION_TOP_DEPTH" => "2",
		"SECTIONS_VIEW_MODE" => "LINE",
		"SECTIONS_SHOW_PARENT_NAME" => "N",
		"PAGE_ELEMENT_COUNT" => "30",
		"LINE_ELEMENT_COUNT" => "3",
		"ELEMENT_SORT_FIELD" => "sort",
		"ELEMENT_SORT_ORDER" => "desc",
		"ELEMENT_SORT_FIELD2" => "id",
		"ELEMENT_SORT_ORDER2" => "desc",
		"LIST_PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"INCLUDE_SUBSECTIONS" => "N",
		"LIST_META_KEYWORDS" => "-",
		"LIST_META_DESCRIPTION" => "-",
		"LIST_BROWSER_TITLE" => "-",
		"DETAIL_PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"DETAIL_META_KEYWORDS" => "-",
		"DETAIL_META_DESCRIPTION" => "-",
		"DETAIL_BROWSER_TITLE" => "-",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"DETAIL_CHECK_SECTION_ID_VARIABLE" => "N",
		"DETAIL_DISPLAY_NAME" => "N",
		"DETAIL_DETAIL_PICTURE_MODE" => "IMG",
		"DETAIL_ADD_DETAIL_TO_SLIDER" => "N",
		"DETAIL_DISPLAY_PREVIEW_TEXT_MODE" => "E",
		"LINK_IBLOCK_TYPE" => "",
		"LINK_IBLOCK_ID" => "",
		"LINK_PROPERTY_SID" => "",
		"LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",
		"USE_STORE" => "N",
		"PAGER_TEMPLATE" => ".default",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"PAGER_TITLE" => "������",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"FILTER_VIEW_MODE" => "VERTICAL",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"COMPONENT_TEMPLATE" => "for_menu",
		"USE_MAIN_ELEMENT_SECTION" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SECTION_BACKGROUND_IMAGE" => "-",
		"DETAIL_SET_CANONICAL_URL" => "N",
		"DETAIL_BACKGROUND_IMAGE" => "-",
		"SHOW_DEACTIVATED" => "N",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"SHOW_404" => "N",
		"MESSAGE_404" => "",
		"DISABLE_INIT_JS_IN_COMPONENT" => "N",
		"SEF_URL_TEMPLATES" => array(
			"sections" => "",
			"section" => "#SECTION_CODE#/",
			"element" => "#SECTION_CODE#/#ELEMENT_CODE#/",
			"compare" => "compare.php?action=#ACTION_CODE#",
			"smart_filter" => "#SECTION_CODE#/filter/#SMART_FILTER_PATH#/apply/",
		),
		"VARIABLE_ALIASES" => array(
			"compare" => array(
				"ACTION_CODE" => "action",
			),
		)
	),
	false
);?>
<?elseif(CSite::InDir('/team/')):?>
<?$APPLICATION->IncludeComponent("bitrix:catalog", "for_menu_t", Array(
	"IBLOCK_TYPE" => "services",	// ��� ���������
		"IBLOCK_ID" => "9",	// ��������
		"SEF_MODE" => "Y",	// �������� ��������� ���
		"SEF_FOLDER" => "/team/",	// ������� ��� (������������ ����� �����)
		"AJAX_MODE" => "N",	// �������� ����� AJAX
		"AJAX_OPTION_JUMP" => "N",	// �������� ��������� � ������ ����������
		"AJAX_OPTION_STYLE" => "Y",	// �������� ��������� ������
		"AJAX_OPTION_HISTORY" => "N",	// �������� �������� ��������� ��������
		"CACHE_TYPE" => "A",	// ��� �����������
		"CACHE_TIME" => "36000000",	// ����� ����������� (���.)
		"CACHE_FILTER" => "N",	// ���������� ��� ������������� �������
		"CACHE_GROUPS" => "Y",	// ��������� ����� �������
		"SET_STATUS_404" => "N",	// ������������� ������ 404
		"SET_TITLE" => "Y",	// ������������� ��������� ��������
		"ADD_SECTIONS_CHAIN" => "Y",	// �������� ������ � ������� ���������
		"ADD_ELEMENT_CHAIN" => "N",	// �������� �������� �������� � ������� ���������
		"USE_ELEMENT_COUNTER" => "N",	// ������������ ������� ����������
		"TEMPLATE_THEME" => "blue",	// �������� ����
		"ADD_PICT_PROP" => "-",	// �������������� �������� ��������� ������
		"LABEL_PROP" => "-",	// �������� ����� ������
		"MESS_BTN_BUY" => "������",	// ����� ������ "������"
		"MESS_BTN_ADD_TO_BASKET" => "� �������",	// ����� ������ "�������� � �������"
		"MESS_BTN_COMPARE" => "���������",	// ����� ������ "���������"
		"MESS_BTN_DETAIL" => "���������",	// ����� ������ "���������"
		"MESS_NOT_AVAILABLE" => "��� � �������",	// ��������� �� ���������� ������
		"DETAIL_USE_VOTE_RATING" => "N",	// �������� ������� ������
		"DETAIL_USE_COMMENTS" => "N",	// �������� ������ � ������
		"DETAIL_BRAND_USE" => "N",	// ������������ ��������� "������"
		"USE_FILTER" => "N",	// ���������� ������
		"ACTION_VARIABLE" => "action",	// �������� ����������, � ������� ���������� ��������
		"PRODUCT_ID_VARIABLE" => "id",	// �������� ����������, � ������� ���������� ��� ������ ��� �������
		"USE_COMPARE" => "N",	// ��������� ��������� �������
		"PRICE_CODE" => "",	// ��� ����
		"USE_PRICE_COUNT" => "N",	// ������������ ����� ��� � �����������
		"SHOW_PRICE_COUNT" => "1",	// �������� ���� ��� ����������
		"PRICE_VAT_INCLUDE" => "Y",	// �������� ��� � ����
		"PRICE_VAT_SHOW_VALUE" => "N",	// ���������� �������� ���
		"BASKET_URL" => "/personal/basket.php",	// URL, ������� �� �������� � �������� ����������
		"USE_PRODUCT_QUANTITY" => "N",	// ��������� �������� ���������� ������
		"ADD_PROPERTIES_TO_BASKET" => "Y",	// ��������� � ������� �������� ������� � �����������
		"PRODUCT_PROPS_VARIABLE" => "prop",	// �������� ����������, � ������� ���������� �������������� ������
		"PARTIAL_PRODUCT_PROPERTIES" => "N",	// ��������� ��������� � ������� ������, � ������� ��������� �� ��� ��������������
		"PRODUCT_PROPERTIES" => "",	// �������������� ������, ����������� � �������
		"SHOW_TOP_ELEMENTS" => "N",	// �������� ��� ���������
		"TOP_ELEMENT_COUNT" => "9",
		"TOP_LINE_ELEMENT_COUNT" => "3",
		"TOP_ELEMENT_SORT_FIELD" => "sort",
		"TOP_ELEMENT_SORT_ORDER" => "asc",
		"TOP_ELEMENT_SORT_FIELD2" => "id",
		"TOP_ELEMENT_SORT_ORDER2" => "desc",
		"TOP_PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"TOP_VIEW_MODE" => "SECTION",
		"SECTION_COUNT_ELEMENTS" => "N",	// ���������� ���������� ��������� � �������
		"SECTION_TOP_DEPTH" => "2",	// ������������ ������������ ������� ��������
		"SECTIONS_VIEW_MODE" => "LINE",	// ��� ������ �����������
		"SECTIONS_SHOW_PARENT_NAME" => "N",	// ���������� �������� �������
		"PAGE_ELEMENT_COUNT" => "30",	// ���������� ��������� �� ��������
		"LINE_ELEMENT_COUNT" => "3",	// ���������� ���������, ��������� � ����� ������ �������
		"ELEMENT_SORT_FIELD" => "sort",	// �� ������ ���� ��������� ������ � �������
		"ELEMENT_SORT_ORDER" => "asc",	// ������� ���������� ������� � �������
		"ELEMENT_SORT_FIELD2" => "id",	// ���� ��� ������ ���������� ������� � �������
		"ELEMENT_SORT_ORDER2" => "desc",	// ������� ������ ���������� ������� � �������
		"LIST_PROPERTY_CODE" => array(	// ��������
			0 => "",
			1 => "",
		),
		"INCLUDE_SUBSECTIONS" => "N",	// ���������� �������� ����������� �������
		"LIST_META_KEYWORDS" => "-",	// ���������� �������� ����� �������� �� �������� �������
		"LIST_META_DESCRIPTION" => "-",	// ���������� �������� �������� �� �������� �������
		"LIST_BROWSER_TITLE" => "-",	// ���������� ��������� ���� �������� �� �������� �������
		"DETAIL_PROPERTY_CODE" => array(	// ��������
			0 => "",
			1 => "",
		),
		"DETAIL_META_KEYWORDS" => "-",	// ���������� �������� ����� �������� �� ��������
		"DETAIL_META_DESCRIPTION" => "-",	// ���������� �������� �������� �� ��������
		"DETAIL_BROWSER_TITLE" => "-",	// ���������� ��������� ���� �������� �� ��������
		"SECTION_ID_VARIABLE" => "SECTION_ID",	// �������� ����������, � ������� ���������� ��� ������
		"DETAIL_CHECK_SECTION_ID_VARIABLE" => "N",	// ������������ ��� ������ �� ����������, ���� �� ����� ������ ��������
		"DETAIL_DISPLAY_NAME" => "N",	// �������� �������� ��������
		"DETAIL_DETAIL_PICTURE_MODE" => "IMG",	// ����� ������ ��������� ��������
		"DETAIL_ADD_DETAIL_TO_SLIDER" => "N",	// ��������� ��������� �������� � �������
		"DETAIL_DISPLAY_PREVIEW_TEXT_MODE" => "E",	// ����� �������� ��� ������ �� ��������� ��������
		"LINK_IBLOCK_TYPE" => "",	// ��� ���������, �������� �������� ������� � ������� ���������
		"LINK_IBLOCK_ID" => "",	// ID ���������, �������� �������� ������� � ������� ���������
		"LINK_PROPERTY_SID" => "",	// ��������, � ������� �������� �����
		"LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",	// URL �� ��������, ��� ����� ������� ������ ��������� ���������
		"USE_STORE" => "N",	// ���������� ���� "���������� ������ �� ������"
		"PAGER_TEMPLATE" => ".default",	// ������ ������������ ���������
		"DISPLAY_TOP_PAGER" => "N",	// �������� ��� �������
		"DISPLAY_BOTTOM_PAGER" => "N",	// �������� ��� �������
		"PAGER_TITLE" => "������",	// �������� ���������
		"PAGER_SHOW_ALWAYS" => "N",	// �������� ������
		"PAGER_DESC_NUMBERING" => "N",	// ������������ �������� ���������
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// ����� ����������� ������� ��� �������� ���������
		"PAGER_SHOW_ALL" => "N",	// ���������� ������ "���"
		"AJAX_OPTION_ADDITIONAL" => "",	// �������������� �������������
		"FILTER_VIEW_MODE" => "VERTICAL",	// ��� ����������� ������ �������
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",	// �������� ����������, � ������� ���������� ���������� ������
		"COMPONENT_TEMPLATE" => "for_menu",
		"USE_MAIN_ELEMENT_SECTION" => "N",	// ������������ �������� ������ ��� ������ ��������
		"SET_LAST_MODIFIED" => "N",	// ������������� � ���������� ������ ����� ����������� ��������
		"SECTION_BACKGROUND_IMAGE" => "-",	// ���������� ������� �������� ��� ������� �� ��������
		"DETAIL_SET_CANONICAL_URL" => "N",	// ������������� ������������ URL
		"DETAIL_BACKGROUND_IMAGE" => "-",	// ���������� ������� �������� ��� ������� �� ��������
		"SHOW_DEACTIVATED" => "N",	// ���������� ���������������� ������
		"PAGER_BASE_LINK_ENABLE" => "N",	// �������� ��������� ������
		"SHOW_404" => "N",	// ����� ����������� ��������
		"MESSAGE_404" => "",	// ��������� ��� ������ (�� ��������� �� ����������)
		"SEF_URL_TEMPLATES" => array(
			"sections" => "",
			"section" => "#SECTION_CODE#/",
			"element" => "#SECTION_CODE#/#ELEMENT_CODE#/",
			"compare" => "compare.php?action=#ACTION_CODE#",
			"smart_filter" => "#SECTION_CODE#/filter/#SMART_FILTER_PATH#/apply/",
		),
		"VARIABLE_ALIASES" => array(
			"compare" => array(
				"ACTION_CODE" => "action",
			),
		)
	),
	false
);?>
<?else:?>
	<?$APPLICATION->IncludeComponent(
		"bitrix:menu", 
		"left", 
		array(
			"ROOT_MENU_TYPE" => "left",
			"MAX_LEVEL" => "1",
			"CHILD_MENU_TYPE" => "left",
			"USE_EXT" => "Y",
			"DELAY" => "N",
			"ALLOW_MULTI_SELECT" => "N",
			"MENU_CACHE_TYPE" => "N",
			"MENU_CACHE_TIME" => "3600",
			"MENU_CACHE_USE_GROUPS" => "Y",
			"MENU_CACHE_GET_VARS" => array(
			)
		),
		false
	);?>
<?endif;?>
				</nav>
				
			</div>
			<div class="content-side">
				<h1>
					<?$APPLICATION->ShowTitle(false)?>
				</h1>
				<?if(CSite::InDir('/services/') or CSite::InDir('/team/')):?> 
					<div class = ""> 
				<?else:?> 
					<div class="content-text"> 
				<?endif;?>