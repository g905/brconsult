$(document).ready(function() {
	$('.fancybox').fancybox();
	
	$('#br_present_name').each(function() {
		 var  default_value = $(this).val();
		 $(this).focus(function(){
			if($(this).val() == default_value) {
			   $(this).val('');
			}
		 });
		 $(this).blur(function(){
			if($(this).val() == '') {
			   $(this).val(default_value);
			}
		 });
	});
	
	$('#br_present_phone').each(function() {
		 var  default_value = $(this).val();
		 $(this).focus(function(){
			if($(this).val() == default_value) {
			   $(this).val('');
			}
		 });
		 $(this).blur(function(){
			if($(this).val() == '') {
			   $(this).val(default_value);
			}
		 });
	});
	
	$('#br_present_org').each(function() {
		 var  default_value = $(this).val();
		 $(this).focus(function(){
			if($(this).val() == default_value) {
			   $(this).val('');
			}
		 });
		 $(this).blur(function(){
			if($(this).val() == '') {
			   $(this).val(default_value);
			}
		 });
	});
});