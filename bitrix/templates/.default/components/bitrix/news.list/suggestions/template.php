<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?foreach($arResult["ITEMS"] as $arItem):?>
	<div class="item">
		<article class="suggestion-article">	
			<div class="suggestion-article-title"><?echo $arItem["NAME"];?></div>
			<?if(!empty($arItem["PREVIEW_PICTURE"]["SRC"])):?>
				<div class="suggestion-article-logo">
					<img src ="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="<?echo $arItem["NAME"]?>">
				</div>
			<?endif;?>
			<div class="suggestion-article-text"><?echo $arItem["PREVIEW_TEXT"];?></div>
		</article>
	</div>
<?endforeach;?>