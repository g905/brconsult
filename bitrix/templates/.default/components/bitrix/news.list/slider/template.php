<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?foreach($arResult["ITEMS"] as $arItem):?>
	<div class="item">
		<article class="review-article">	
		<div id="popap_el_top_sl"></div>
			<?if(!empty($arItem["PREVIEW_PICTURE"]["SRC"])):?>
				<div class="review-article-logo">
					<img src ="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="<?echo $arItem["NAME"]?>">
				</div>
			<?endif;?>
			<div class="review-article-text"><?echo $arItem["PREVIEW_TEXT"];?></div>
			<div class="review-article-button">
				<div id="detail_ajax">
					<!--<a class="button-transparent fancybox fancybox.ajax load_id" id="load_id" href="/include/detail.php?ELEMENT_ID=<?//=$arItem["ID"]?>#yakor">������ ����� ���������</a>-->
					<a class="button-transparent load_id" id="load_id_<?=$arItem["ID"]?>" href="javascript:void(0);">������ ����� ���������</a>
					<script type="text/javascript">
						$(document).ready(function() {
							$('#load_id_<?=$arItem["ID"]?>').click(function() {
								$('#for_fanc').load("/include/detail.php?ELEMENT_ID=<?=$arItem["ID"]?>#yakor");
								$('#for_fanc_block').fadeIn(1000);
								$('#for_fanc').fadeIn(1500);
							});
						});
					</script>
				</div>
			</div>
		<div id="popap_el_bot_sl"></div>
		</article>
	</div>
<?endforeach;?>