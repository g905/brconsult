<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?$cnt=0;?>
<?foreach($arResult["ITEMS"] as $arItem):?>
<?$cnt++;?>
	<div class="service-card"> 				
		<section class="card"> 					
			<div class="front"> 						
				<div class="service-icon"><img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"></div>
				<div class="service-name"><?echo $arItem["NAME"]?></div>
				<a href="<?echo $arItem["PROPERTIES"]["url"]["VALUE"]?>" class="button-white" >Подробнее</a>
			</div> 
		</section>
	</div>
<?endforeach;?>
