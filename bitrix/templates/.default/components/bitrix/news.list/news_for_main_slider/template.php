<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?foreach($arResult["ITEMS"] as $arItem):?>
<?if(!empty($arItem["PREVIEW_PICTURE"]["SRC"])):?>
	<div class="item m-news-item">
		<article class="suggestion-article">				
			<div class="suggestion-article-logo news-logo" style="background-image: url(<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>);"></div>
				
			<div class="m-news-date">
				<?if($arParams["DISPLAY_DATE"]!="N" && $arItem["DISPLAY_ACTIVE_FROM"]):?>
					<?
					$rest = substr($arItem["DISPLAY_ACTIVE_FROM"], 0, -5);
					$exp = explode(" ",$rest);
					?>
					<span class="m-news-date-time">
						<span class="d1"><?=$exp[0]?></span>
						<span class="d2"><?=$exp[1]?></span>
					</span>
				<?endif?>
			</div>	

			<div class="m-news-content">
				<div class="suggestion-article-title m-news-item-title"><?echo $arItem["NAME"];?></div>	
				<div class="m-news-item-text"><?echo $arItem["PREVIEW_TEXT"];?></div>	
				<a href="<?echo $arItem["DETAIL_PAGE_URL"];?>" class="m-news-item-more">������ �����</a>
			</div>
				
		</article>
	</div>
 <?endif;?>
<?endforeach;?>