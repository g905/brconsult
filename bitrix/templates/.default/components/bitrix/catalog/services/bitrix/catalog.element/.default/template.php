<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if(CSite::InDir('/team/nashi-dostizheniya/')):?>
	<div id="gallery">
		<?
		$e=CIBlockElement::GetProperty($arResult["IBLOCK_ID"],$arResult['ID'],array(),array("CODE"=>"IMAGES"));
		while($xx=$e->Fetch())
		{   if($x=CFile::GetFileArray($xx['VALUE']))
			{
				$y=CFile::ResizeImageGet(
					$xx['VALUE'],
					array("width" => 150, "height" => 150),
					BX_RESIZE_IMAGE_EXACT,
					false
				);
				$k=$xx["DESCRIPTION"];
				// echo'<a title="'.$k.'" rel="example_group" href="'.$x['SRC'].'"><img src="'.$y["src"].'" alt="'.$k.'" title="'.$k.'"/></a>';
				echo'<a title="'.$k.'" class="fancybox-buttons" data-fancybox-group="button" href="'.$x['SRC'].'"><img src="'.$y["src"].'" alt="'.$k.'" title="'.$k.'"/></a>';
			}
		}
		?>
	</div>
<?else:?>
	<div style="margin-left: 30px;">
	<?if(!empty($arResult['DETAIL_PICTURE']['SRC'])):?><img style="float: left; margin-right: 20px; margin-bottom: 20px;" src="<?=$arResult['DETAIL_PICTURE']['SRC']?>"><?endif;?>
	<?=$arResult['DETAIL_TEXT'];?>
	</div>
<?endif;?>
