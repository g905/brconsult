<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$arViewModeList = $arResult['VIEW_MODE_LIST'];

$arViewStyles = array(
	'LIST' => array(
		'CONT' => 'bx_sitemap',
		'TITLE' => 'bx_sitemap_title',
		'LIST' => 'bx_sitemap_ul',
	),
	'LINE' => array(
		'CONT' => 'bx_catalog_line',
		'TITLE' => 'bx_catalog_line_category_title',
		'LIST' => 'bx_catalog_line_ul',
		'EMPTY_IMG' => $this->GetFolder().'/images/line-empty.png'
	),
	'TEXT' => array(
		'CONT' => 'bx_catalog_text',
		'TITLE' => 'bx_catalog_text_category_title',
		'LIST' => 'bx_catalog_text_ul'
	),
	'TILE' => array(
		'CONT' => 'bx_catalog_tile',
		'TITLE' => 'bx_catalog_tile_category_title',
		'LIST' => 'bx_catalog_tile_ul',
		'EMPTY_IMG' => $this->GetFolder().'/images/tile-empty.png'
	)
);
$arCurView = $arViewStyles[$arParams['VIEW_MODE']];

$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));

?><div class="<? echo $arCurView['CONT']; ?>"><?
if ('Y' == $arParams['SHOW_PARENT_NAME'] && 0 < $arResult['SECTION']['ID'])
{
	$this->AddEditAction($arResult['SECTION']['ID'], $arResult['SECTION']['EDIT_LINK'], $strSectionEdit);
	$this->AddDeleteAction($arResult['SECTION']['ID'], $arResult['SECTION']['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);

	?><h1
		class="<? echo $arCurView['TITLE']; ?>"
		id="<? echo $this->GetEditAreaId($arResult['SECTION']['ID']); ?>"
	><a href="<? echo $arResult['SECTION']['SECTION_PAGE_URL']; ?>"><?
		echo (
			isset($arResult['SECTION']["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"]) && $arResult['SECTION']["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"] != ""
			? $arResult['SECTION']["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"]
			: $arResult['SECTION']['NAME']
		);
	?></a></h1><?
}
if (0 < $arResult["SECTIONS_COUNT"])
{
?>
<ul class="bg-li-none <? echo $arCurView['LIST']; ?> e-br-cats-ul">
<?
	switch ($arParams['VIEW_MODE'])
	{
		case 'LINE':
			$cnt = 0;
			foreach ($arResult['SECTIONS'] as &$arSection)
			{
				$cnt++;

				$this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
				$this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);

				?>
				<li id="<? echo $this->GetEditAreaId($arSection['ID']); ?>">

				<?if ('' != $arSection['DESCRIPTION']):?>

					<div class="page-content <?if($cnt%2):?> page-content-white <?else:?> page-content-grey <?endif;?> ">
					<div class="page-content-h2">
					<div id="<? echo $arSection['CODE']; ?>" class="service-scroll"></div>
					  <h2><? echo $arSection['NAME']; ?><br /></h2>

					  <div class="page-section-logo-area"><?if(!empty($arSection['PICTURE']['SRC'])):?><img id="bxid_326119" src="<? echo $arSection['PICTURE']['SRC']; ?>" class="page-section-logo" width="60" height="60" /><?endif;?>
						<div <?if(empty($arSection['PICTURE']['SRC'])):?>style="margin-left: 0px; width: 710px;"<?endif;?> class="page-section-grey-line <?if($cnt%2):?> grey-1 <?else:?> grey-2 <?endif;?> "></div>
					   </div>

					  <div class="page-content-text">
					   <? echo $arSection['DESCRIPTION']; ?>
					 </div>

					 <?
					 $arFilter = array("IBLOCK_ID"=>$arSection["IBLOCK_ID"], "ID"=>$arSection["ID"]);
					 $db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, true, array("UF_BUTTON"));
					 while($ar_result = $db_list->GetNext())
					 {
						if($ar_result["UF_BUTTON"] == 1){
							?><a href="<? echo $arSection['SECTION_PAGE_URL']; ?>" class="dop_menu_services" >Подробнее</a><?
						}
					 }
					 ?>

					</div>

				<?endif;?>

				<div style="clear: both;"></div>
				</li><?
			}
			unset($arSection);
		break;
		case 'TILE':
			$cnt = 0;
			?>

			<table class="e-br-cats-table">
				<?
				$cnt2 = 0;
	 		foreach ($arResult['SECTIONS'] as &$arSection)
			{
				$cnt++;
				//echo "<pre>";
				//print_r($arSection);
				//echo "</pre>";
?>
<?if (!fmod($cnt2, 2)):?>
	<tr class="e-br-row">
<?endif;?>
<td>
	<div class = "e-br-cat-wrap">
		<div class = "e-br-cat-icon">
			<img src="<?=$arSection['PICTURE']['SRC']?>">
		</div>
		<div class = "e-br-cat-name">
			<?=$arSection["NAME"]?>
		</div>
		<div class = "e-br-cat-desc">
			<?=$arSection["DESCRIPTION"]?>
		</div>
		<div class = "e-br-cat-details">
			<a class = "e-br-cat-link" href="<?=$arSection['SECTION_PAGE_URL']?>">Подробнее</a>

		</div>
	</div>


</td>
<?if (fmod($cnt2, 2)):?>
	</tr>
<?endif;?>

<?
$cnt2++;
			}
			?></table><?
	 		break;
	}
?>
</ul>
<?
	echo ('LINE' != $arParams['VIEW_MODE'] ? '<div style="clear: both;"></div>' : '');
}
?></div>
