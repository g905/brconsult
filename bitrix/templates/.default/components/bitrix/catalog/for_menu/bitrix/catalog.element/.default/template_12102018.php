<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<ul class="nav-side-menu">
<?
if(CModule::IncludeModule("iblock"))
{
	$arSelect = Array("IBLOCK_SECTION_ID");
	$arFilter = Array("IBLOCK_ID"=>7, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "ID"=>$arResult["ID"]);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
	if($ob = $res->GetNextElement())
	{
		$arFields = $ob->GetFields();

		$arSelect_menu = Array("NAME", "CODE", "DETAIL_PAGE_URL");
		$arFilter_menu = Array("IBLOCK_ID"=>7, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "SECTION_ID"=>$arFields["IBLOCK_SECTION_ID"]);
		$res_menu = CIBlockElement::GetList(Array("id" => "desc"), $arFilter_menu, false, false, $arSelect_menu);
		while($ob_menu = $res_menu->GetNextElement())
		{
			$arFields_menu = $ob_menu->GetFields();
			$exp = explode("/",$arFields_menu["DETAIL_PAGE_URL"]);

			for($i = 0; $i < count($exp)-2; $i++){
				if(!empty($exp[$i])){
					$url = "/".$exp[$i];
				}
			}

			?>
			<li>
				<a href="<? echo "/services".$url."/#".$arFields_menu["CODE"]; ?>" class="nav-side-menu-item"><? echo $arFields_menu['NAME']; ?></a>
			</li>
			<?
		}
	}
}
?>
</ul>
