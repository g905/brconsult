<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if (!empty($arResult['ITEMS']))
{
?>
<ul class="nav-side-menu">
<?
	foreach ($arResult['ITEMS'] as $arItem)
	{
		?>
		<li>
			<a href="#<? echo $arItem['CODE']; ?>" class="nav-side-menu-item"><? echo $arItem['NAME']; ?></a>
		</li>
		<?
	}
?></ul><?
}
?>

<script type="text/javascript">
	$(document).ready(function() {			  
		$(".nav-side-menu").on("click","a", function (event) {
			
			$('.nav-side-menu li a').each(function(){
				$(this).parent().removeClass('active');
			});
			$(this).parent().addClass('active');
			
			event.preventDefault();
			var id  = $(this).attr('href'),
				top = $(id).offset().top;
			$('body,html').animate({scrollTop: top}, 1000);
		});
	});
</script>