<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?
if(CSite::InDir('/services/')) {
  $ib_code = 'service';
} else if (CSite::InDir('/team/')) {
  $ib_code = 'team';
} else if (CSite::InDir('/reviews/')) {
  $ib_code = 'corporate_reviews';
}

$sections = CIBlockSection::GetList(
	Array("SORT"=>"ASC"),
	Array("ACTIVE"=>"Y", "DEPTH_LEVEL"=>"1", "IBLOCK_CODE"=>$ib_code),
	false,
	Array(),
	false
);

while ($s = $sections->GetNext()) {
	//print_r($s);
	$arElements = [];
	$elements = CIBlockElement::GetList(
		Array("DATE"=>"ASC"),
		Array("ACTIVE"=>"Y", "SECTION_ID"=>$s["ID"]),
		false,
		Array(),
		false
	);
	while ($element = $elements->GetNext()) {
		$arElements[] = $element;
	}
	$sects[] = Array("NAME"=>$s["NAME"], "LINK"=>$s["SECTION_PAGE_URL"], "CHILDREN"=>$arElements);
}

/*echo "<pre>";
print_r($sects);
echo "</pre>";*/
if (0 < count($sects)) {
?>
<style>
.nav-side {
	/*position: absolute;*/
  position: fixed;
	font-family: 'Roboto', 'Verdana', 'sans-serif';
}
ul.nav-side-menu li {
	background: none;
	padding-left: 0;
	display: flex;
	flex-wrap: wrap;
  padding-bottom: 0px;
}
ul.nav-side-menu li:hover .dot:not(.e-active) {
	background-color: #ddf;
}
.e-line-wrap {
	display: contents;
}
.dot {
	width: 10px;
	height: 10px;
	border: 2px solid #0049d3;
	border-radius: 10px;
	display: inline-block;
	vertical-align: middle;
}
.e-active {
	background-color: #0049d3;
}
.dot-wrap {
	height: 100%;
	width: 10px;
	display: inline-block;
	vertical-align: middle;
}
.cont {
	display: inline-block;
	margin-left: 10px;
	width: 80%;
}
.cont a {
	color: #232323!important;
	font-weight: normal;
}
ul.e-nav-side-menu-2 {
	font-family: 'Roboto', 'Verdana', 'sans-serif';
    list-style-type: none;
    padding: 0px;
    margin: 0px;
    text-transform: none;
    font-size: 13px;
    position: relative;
    /*display: none;*/
}

ul.e-nav-side-menu-2 li {
    padding: 5px 10px 0px 40px;
    cursor: pointer;
    background-image: none;
    display: inline-block;
}
ul.e-nav-side-menu-2 li:before {
	content: '';
	display: inline-block;
	border-radius: 3px;
	width: 6px;
	height: 6px;
	background: #0049d3;
}

.e-space {
  display: block;
  height: 100px;
  position: relative;
}
</style>
<? $sectname =  trim(GetPagePath())?>

<ul class = "nav-side-menu">
	<?foreach ($sects as $sect): ?>
		<li>
			<div class = "e-line-wrap">
				<div class = "dot-wrap">
					<?if ($sectname == trim($sect['LINK'])):?>
						<div class = "dot e-active"></div>
					<?else:?>
						<div class = "dot"></div>
					<?endif;?>
				</div>
				<div class = "cont">
					<a href="<?=$sect['LINK']?>" onclick = "window.location.href='<?=$sect['LINK']?>'"><?=$sect["NAME"];?></a>
				</div>
			</div>

			<?if ($sectname == trim($sect['LINK'])):?>

				<?if (0 < count($sect["CHILDREN"])):?>

					<ul class = "e-nav-side-menu-2">
						<?foreach($sect["CHILDREN"] as $l2):?>
							<li>
								<a href="#<?=$l2['CODE']?>" class="nav-side-menu-item"><?=$l2["NAME"]?></a>
							</li>
						<?endforeach;?>
					</ul>
				<?endif;?>
			<?endif;?>
		</li>
	<?endforeach; ?>
</ul>

<?}?>

<?/*
if (!empty($arResult['ITEMS']))
{
?>
<ul class="nav-side-menu">
<?
	foreach ($arResult['ITEMS'] as $arItem)
	{

		?>
		<li>
			<a href="#<? echo $arItem['CODE']; ?>" class="nav-side-menu-item"><? echo $arItem['NAME']; ?></a>
		</li>
		<?
	}
?></ul><?
}
*/?>

<script type="text/javascript">
	$(document).ready(function() {
		$(".nav-side-menu").on("click","a", function (event) {

			$('.nav-side-menu li a').each(function(){
				$(this).parent().removeClass('active');
			});
			$(this).parent().addClass('active');

			event.preventDefault();
			var id  = $(this).attr('href'),
				top = $(id).offset().top;
			$('body,html').animate({scrollTop: top}, 1000);
		});
	});
</script>
