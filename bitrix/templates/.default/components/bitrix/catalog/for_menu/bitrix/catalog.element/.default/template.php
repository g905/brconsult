<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

?>

<style>
.nav-side {
	/*position: absolute;*/
	font-family: 'Roboto', 'Verdana', 'sans-serif';
}

.nav-side::after {
  content: '';
  display: block;
  height: 100px;
  position: fixed;
}
ul.nav-side-menu li {
	background: none;
	padding-left: 0;
	display: flex;
	flex-wrap: wrap;
}
ul.nav-side-menu li:hover .dot {
	background-color: #ddf;
}
.e-line-wrap {
	display: contents;
}
.dot {
	width: 10px;
	height: 10px;
	border: 2px solid #0049d3;
	border-radius: 10px;
	display: inline-block;
	vertical-align: middle;
}
.dot-wrap {
	height: 100%;
	width: 10px;
	display: inline-block;
	vertical-align: middle;
}
.cont {
	display: inline-block;
	margin-left: 10px;
	width: 80%;
}
.cont a {
	color: #232323!important;
	font-weight: normal;
}
ul.e-nav-side-menu-2 {
	font-family: 'Roboto', 'Verdana', 'sans-serif';
    list-style-type: none;
    padding: 0px;
    margin: 0px;
    text-transform: none;
    font-size: 13px;
    position: relative;
    /*display: none;*/
}

ul.e-nav-side-menu-2 li {
    padding: 5px 10px 5px 40px;
    cursor: pointer;
    background-image: none;
    display: inline-block;
}
ul.e-nav-side-menu-2 li:before {
	content: '';
	display: inline-block;
	border-radius: 3px;
	width: 6px;
	height: 6px;
	background: #0049d3;
}

</style>
<pre>
  <?//print_r($arResult)?>
</pre>
<?
if ($arResult)
{
?>

<?
$sections = CIBlockSection::GetList(
  Array("SORT"=>"ASC"),
  Array("ACTIVE"=>"Y", "DEPTH_LEVEL"=>"1", "IBLOCK_CODE"=>$arResult["SECTIONS"][0]["IBLOCK_CODE"]),
  false,
  Array(),
  false
);

while ($s = $sections->GetNext()) {
	//print_r($s);
	$arElements = [];
	$elements = CIBlockElement::GetList(
		Array("DATE"=>"ASC"),
		Array("ACTIVE"=>"Y", "SECTION_ID"=>$s["ID"]),
		false,
		Array(),
		false
	);
	while ($element = $elements->GetNext()) {
		$arElements[] = $element;
	}
  $se = $arElements[0]["DETAIL_PAGE_URL"];
	$sects[] = Array("NAME"=>$s["NAME"], "LINK"=>$se, "CODE"=>$s["CODE"], "PARENT"=>$s["LIST_PAGE_URL"], "CHILDREN"=>$arElements);
}

foreach($sects as $s) {
  //echo $s["CHILDREN"][0]["NAME"]."<br>";
}

?>
<? $sectname =  trim(GetPagePath())?>

<ul class = "nav-side-menu">
	<?foreach ($sects as $sect): ?>

		<li>
			<div class = "e-line-wrap">
				<div class = "dot-wrap">
					<?if ($sectname == trim($sect['LINK'])):?>
						<div class = "dot e-active"></div>
					<?else:?>
						<div class = "dot"></div>
					<?endif;?>
				</div>
				<div class = "cont">
          <?if (0 >= count($sect["CHILDREN"])):?>

              <?if ($sect["PARENT"] !== trim(GetPagePath())):?>
					         <a href="#<?=$sect['CODE']?>" onclick = "window.location.href='<?=$sect['PARENT']?>#<?=$sect['CODE']?>'"><?=$sect["NAME"];?></a>
              <?else:?>
                  <a href="#<?=$sect['CODE']?>"><?=$sect["NAME"];?></a>
              <?endif;?>
          <?else:?>
               <a href="<?=$sect['LINK']?>" onclick = "window.location.href='<?=$sect['LINK']?>'"><?=$sect["NAME"];?></a>
          <?endif;?>
				</div>
			</div>

			<?if ($sectname == trim($sect['LINK'])):?>

				<?if (0 < count($sect["CHILDREN"])):?>

					<ul class = "e-nav-side-menu-2">
						<?foreach($sect["CHILDREN"] as $l2):?>
							<li>
								<a href="#<?=$l2['CODE']?>" class="nav-side-menu-item"><?=$l2["NAME"]?></a>
							</li>
						<?endforeach;?>
					</ul>
				<?endif;?>
			<?endif;?>
		</li>
	<?endforeach; ?>
</ul>

<?
}

if(CSite::InDir('/reviews/')) {
  	$arElements = [];
  	$elements = CIBlockElement::GetList(
  		Array("DATE"=>"ASC"),
  		Array("ACTIVE"=>"Y", "IBLOCK_CODE"=>"corporate_reviews"),
  		false,
  		Array(),
  		false
  	);
  	while ($element = $elements->GetNext()) {
  		$arElements[] = Array("NAME"=>$element["NAME"], "CODE"=>$element["CODE"], "LINK"=>$element["DETAIL_PAGE_URL"]);
  	}
?>
<ul class = "nav-side-menu">
	<?foreach ($arElements as $el): ?>
		<li>
			<div class = "e-line-wrap">
				<div class = "dot-wrap">
						<div class = "dot"></div>
				</div>
				<div class = "cont">
          <a href="#<?=$el['CODE']?>"><?=$el["NAME"];?></a>
				</div>
			</div>
		</li>
	<?endforeach; ?>
</ul>
<?
}
?>

<script type="text/javascript">
	$(document).ready(function() {
		$(".nav-side-menu").on("click","a", function (event) {

			$('.nav-side-menu li a').each(function(){
				$(this).parent().removeClass('active');
			});
			$(this).parent().addClass('active');

			event.preventDefault();
			var id  = $(this).attr('href'),
				top = $(id).offset().top;
			$('body,html').animate({scrollTop: top}, 1000);
		});
	});
</script>
