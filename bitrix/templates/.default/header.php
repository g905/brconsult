<!DOCTYPE html>

<!--
  Copyright (c) ÎÎÎ "ÁÐ Êîíñàëò" 2015
  http://www.brconsult.pro/

-->

<html>
    <head>
        <?$APPLICATION->ShowHead();?>
		<title><?$APPLICATION->ShowTitle();?></title>
		<link rel="icon" href="http://brconsult.pro/favicon.ico" type="image/x-icon">
		<link rel="shortcut icon" href="<?=SITE_TEMPLATE_PATH?>/favicon.ico">

		<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/lib/jquery-1.10.1.min.js"></script>
		<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/script.js"></script>

		<?if(CSite::InDir('/team/nashi-dostizheniya/')):?>
		<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/fancybox/jquery.fancybox-1.3.1.js"></script>
		<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/js/fancybox/jquery.fancybox-1.3.1.css" media="screen" />
		<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/lib/jquery.mousewheel-3.0.6.pack.js"></script>
		<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/source/jquery.fancybox.js?v=2.1.5"></script>
		<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/js/source/jquery.fancybox.css?v=2.1.5" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/js/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" />
		<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
		<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/js/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />
		<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
		<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>
		<?else:?>
		<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/fancybox/jquery.fancybox-1.3.1.js"></script>
		<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/js/fancybox/jquery.fancybox-1.3.1.css" media="screen" />
		<?endif;?>
		<link href="http://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css">

		<!-- Yandex.Metrika counter -->
		<script type="text/javascript">
			(function (d, w, c) {
				(w[c] = w[c] || []).push(function() {
					try {
						w.yaCounter36646360 = new Ya.Metrika({
							id:36646360,
							clickmap:true,
							trackLinks:true,
							accurateTrackBounce:true,
							webvisor:true,
							trackHash:true
						});
					} catch(e) { }
				});

				var n = d.getElementsByTagName("script")[0],
					s = d.createElement("script"),
					f = function () { n.parentNode.insertBefore(s, n); };
				s.type = "text/javascript";
				s.async = true;
				s.src = "https://mc.yandex.ru/metrika/watch.js";

				if (w.opera == "[object Opera]") {
					d.addEventListener("DOMContentLoaded", f, false);
				} else { f(); }
			})(document, window, "yandex_metrika_callbacks");
		</script>
		<noscript><div><img src="https://mc.yandex.ru/watch/36646360" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
		<!-- /Yandex.Metrika counter -->
		<script type="text/javascript">
			$(function() {
				$('a.popup').fancybox({
					'overlayShow': true,
					'padding': 30,
					'margin' : 20,
					'scrolling' : 'no',
					'titleShow': false,
					'type': 'ajax',
					'href': '/include/quest.php'
				});
				$('a.bid_ed').fancybox({
					'overlayShow': true,
					'padding': 30,
					'margin' : 20,
					'scrolling' : 'no',
					'titleShow': false,
					'type': 'ajax',
					'href': '/include/bid.php?srv=ÝËÅÊÒÐÎÍÍÛÉ ÄÎÊÓÌÅÍÒÎÎÁÎÐÎÒ'
				});
				$('a.bid_ab').fancybox({
					'overlayShow': true,
					'padding': 30,
					'margin' : 20,
					'scrolling' : 'no',
					'titleShow': false,
					'type': 'ajax',
					'href': '/include/bid.php?srv=ÀÂÒÎÌÀÒÈÇÀÖÈß ÁÓÕÃÀËÒÅÐÈÈ'
				});
				$('a.bid_adp').fancybox({
					'overlayShow': true,
					'padding': 30,
					'margin' : 20,
					'scrolling' : 'no',
					'titleShow': false,
					'type': 'ajax',
					'href': '/include/bid.php?srv=ÀÍÀËÈÇ ÄÀÍÍÛÕ È ÏÐÎÃÍÎÇÈÐÎÂÀÍÈÅ'
				});
			});

			// $(document).ready(function() {
				// $(".nav-side-menu").on("click","a", function (event) {
					// event.preventDefault();
					// var id  = $(this).attr('href'),
						// top = $(id).offset().top;
					// $('body,html').animate({scrollTop: top}, 1000);
				// });
			// });
		</script>

		<?//if($_SERVER["REQUEST_URI"] == "/team/"):?>
			<script type="text/javascript">
				/* $(document).ready(function() {
					$(".nav-side-menu").on("click","a", function (event) {

						$('.nav-side-menu li a').each(function(){
							$(this).parent().removeClass('active');
						});
						$(this).parent().addClass('active');

						event.preventDefault();
						var id  = $(this).attr('href'),
							top = $(id).offset().top;
						$('body,html').animate({scrollTop: top}, 1000);
					});
				}); */
			</script>
		<?//endif;?>

		<script type="text/javascript" src="//vk.com/js/api/openapi.js?127"></script>

		<!-- VK Widget -->
		<div id="vk_community_messages"></div>
		<script type="text/javascript">
			VK.Widgets.CommunityMessages("vk_community_messages", 104103147, {shown: 0});
		</script>

		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-84795134-1', 'auto');
		  ga('send', 'pageview');

		</script>


		<!--[if IE]>
			<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/ie.css" />
		<![endif]-->

		<!--[if lt IE 9]>
			<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/reject/reject.css" media="all" />
			<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/reject/reject.min.js"></script>
		<![endif]-->

		<?CUtil::InitJSCore(Array("ajax"));?>
    </head>
	<?$APPLICATION->ShowPanel();?>

    <body>
		<header>
			<div class="header-area section">
				<div class="wrap">
					<a href="/"><div class="logo"></div></a>
					<?$APPLICATION->IncludeComponent(
"bitrix:menu",
"br_main_menu",
Array(
	"ROOT_MENU_TYPE" => "top",
	"MAX_LEVEL" => "1",
	"CHILD_MENU_TYPE" => "left",
	"USE_EXT" => "N",
	"DELAY" => "N",
	"ALLOW_MULTI_SELECT" => "N",
	"MENU_CACHE_TYPE" => "N",
	"MENU_CACHE_TIME" => "3600",
	"MENU_CACHE_USE_GROUPS" => "Y",
	"MENU_CACHE_GET_VARS" => array()
),
false
);?>
					<div class="phone-header">
						<div class="phone-image"></div>
						<div class="phone-number">+7 3412</div>
						<div class="phone-number blue"> 95 88 11</div><br>
					</div>
				</div>
			</div>
		</header>
	<section class="content2"></section>
	<section class="content">
		<div class="wrap">
			<div class="nav-side">
				<div class="nav-title">Íàâèãàöèÿ ïî ðàçäåëó</div>
				<div class="nav-border"></div>

				<nav>
<?if(CSite::InDir('/services/')):?>
<?$APPLICATION->IncludeComponent(
	"bitrix:catalog",
	"for_menu",
	array(
		"IBLOCK_TYPE" => "services",
		"IBLOCK_ID" => "7",
		"SEF_MODE" => "Y",
		"SEF_FOLDER" => "/services/",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"ADD_ELEMENT_CHAIN" => "N",
		"USE_ELEMENT_COUNTER" => "N",
		"TEMPLATE_THEME" => "blue",
		"ADD_PICT_PROP" => "-",
		"LABEL_PROP" => "-",
		"MESS_BTN_BUY" => "Êóïèòü",
		"MESS_BTN_ADD_TO_BASKET" => "Â êîðçèíó",
		"MESS_BTN_COMPARE" => "Ñðàâíåíèå",
		"MESS_BTN_DETAIL" => "Ïîäðîáíåå",
		"MESS_NOT_AVAILABLE" => "Íåò â íàëè÷èè",
		"DETAIL_USE_VOTE_RATING" => "N",
		"DETAIL_USE_COMMENTS" => "N",
		"DETAIL_BRAND_USE" => "N",
		"USE_FILTER" => "N",
		"ACTION_VARIABLE" => "action",
		"PRODUCT_ID_VARIABLE" => "id",
		"USE_COMPARE" => "N",
		"PRICE_CODE" => array(
		),
		"USE_PRICE_COUNT" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"PRICE_VAT_INCLUDE" => "Y",
		"PRICE_VAT_SHOW_VALUE" => "N",
		"BASKET_URL" => "/personal/basket.php",
		"USE_PRODUCT_QUANTITY" => "N",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRODUCT_PROPERTIES" => array(
		),
		"SHOW_TOP_ELEMENTS" => "N",
		"TOP_ELEMENT_COUNT" => "9",
		"TOP_LINE_ELEMENT_COUNT" => "3",
		"TOP_ELEMENT_SORT_FIELD" => "sort",
		"TOP_ELEMENT_SORT_ORDER" => "asc",
		"TOP_ELEMENT_SORT_FIELD2" => "id",
		"TOP_ELEMENT_SORT_ORDER2" => "desc",
		"TOP_PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"TOP_VIEW_MODE" => "SECTION",
		"SECTION_COUNT_ELEMENTS" => "N",
		"SECTION_TOP_DEPTH" => "2",
		"SECTIONS_VIEW_MODE" => "LINE",
		"SECTIONS_SHOW_PARENT_NAME" => "N",
		"PAGE_ELEMENT_COUNT" => "30",
		"LINE_ELEMENT_COUNT" => "3",
		"ELEMENT_SORT_FIELD" => "sort",
		"ELEMENT_SORT_ORDER" => "desc",
		"ELEMENT_SORT_FIELD2" => "id",
		"ELEMENT_SORT_ORDER2" => "desc",
		"LIST_PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"INCLUDE_SUBSECTIONS" => "N",
		"LIST_META_KEYWORDS" => "-",
		"LIST_META_DESCRIPTION" => "-",
		"LIST_BROWSER_TITLE" => "-",
		"DETAIL_PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"DETAIL_META_KEYWORDS" => "-",
		"DETAIL_META_DESCRIPTION" => "-",
		"DETAIL_BROWSER_TITLE" => "-",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"DETAIL_CHECK_SECTION_ID_VARIABLE" => "N",
		"DETAIL_DISPLAY_NAME" => "N",
		"DETAIL_DETAIL_PICTURE_MODE" => "IMG",
		"DETAIL_ADD_DETAIL_TO_SLIDER" => "N",
		"DETAIL_DISPLAY_PREVIEW_TEXT_MODE" => "E",
		"LINK_IBLOCK_TYPE" => "",
		"LINK_IBLOCK_ID" => "",
		"LINK_PROPERTY_SID" => "",
		"LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",
		"USE_STORE" => "N",
		"PAGER_TEMPLATE" => ".default",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"PAGER_TITLE" => "Òîâàðû",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"FILTER_VIEW_MODE" => "VERTICAL",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"COMPONENT_TEMPLATE" => "for_menu",
		"USE_MAIN_ELEMENT_SECTION" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SECTION_BACKGROUND_IMAGE" => "-",
		"DETAIL_SET_CANONICAL_URL" => "N",
		"DETAIL_BACKGROUND_IMAGE" => "-",
		"SHOW_DEACTIVATED" => "N",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"SHOW_404" => "N",
		"MESSAGE_404" => "",
		"DISABLE_INIT_JS_IN_COMPONENT" => "N",
		"SEF_URL_TEMPLATES" => array(
			"sections" => "",
			"section" => "#SECTION_CODE#/",
			"element" => "#SECTION_CODE#/#ELEMENT_CODE#/",
			"compare" => "compare.php?action=#ACTION_CODE#",
			"smart_filter" => "#SECTION_CODE#/filter/#SMART_FILTER_PATH#/apply/",
		),
		"VARIABLE_ALIASES" => array(
			"compare" => array(
				"ACTION_CODE" => "action",
			),
		)
	),
	false
);?>
<?elseif(CSite::InDir('/team/')):?>
<?$APPLICATION->IncludeComponent("bitrix:catalog", "for_menu_t", Array(
	"IBLOCK_TYPE" => "services",	// Òèï èíôîáëîêà
		"IBLOCK_ID" => "9",	// Èíôîáëîê
		"SEF_MODE" => "Y",	// Âêëþ÷èòü ïîääåðæêó ×ÏÓ
		"SEF_FOLDER" => "/team/",	// Êàòàëîã ×ÏÓ (îòíîñèòåëüíî êîðíÿ ñàéòà)
		"AJAX_MODE" => "N",	// Âêëþ÷èòü ðåæèì AJAX
		"AJAX_OPTION_JUMP" => "N",	// Âêëþ÷èòü ïðîêðóòêó ê íà÷àëó êîìïîíåíòà
		"AJAX_OPTION_STYLE" => "Y",	// Âêëþ÷èòü ïîäãðóçêó ñòèëåé
		"AJAX_OPTION_HISTORY" => "N",	// Âêëþ÷èòü ýìóëÿöèþ íàâèãàöèè áðàóçåðà
		"CACHE_TYPE" => "A",	// Òèï êåøèðîâàíèÿ
		"CACHE_TIME" => "36000000",	// Âðåìÿ êåøèðîâàíèÿ (ñåê.)
		"CACHE_FILTER" => "N",	// Êåøèðîâàòü ïðè óñòàíîâëåííîì ôèëüòðå
		"CACHE_GROUPS" => "Y",	// Ó÷èòûâàòü ïðàâà äîñòóïà
		"SET_STATUS_404" => "N",	// Óñòàíàâëèâàòü ñòàòóñ 404
		"SET_TITLE" => "Y",	// Óñòàíàâëèâàòü çàãîëîâîê ñòðàíèöû
		"ADD_SECTIONS_CHAIN" => "Y",	// Âêëþ÷àòü ðàçäåë â öåïî÷êó íàâèãàöèè
		"ADD_ELEMENT_CHAIN" => "N",	// Âêëþ÷àòü íàçâàíèå ýëåìåíòà â öåïî÷êó íàâèãàöèè
		"USE_ELEMENT_COUNTER" => "N",	// Èñïîëüçîâàòü ñ÷åò÷èê ïðîñìîòðîâ
		"TEMPLATE_THEME" => "blue",	// Öâåòîâàÿ òåìà
		"ADD_PICT_PROP" => "-",	// Äîïîëíèòåëüíàÿ êàðòèíêà îñíîâíîãî òîâàðà
		"LABEL_PROP" => "-",	// Ñâîéñòâî ìåòîê òîâàðà
		"MESS_BTN_BUY" => "Êóïèòü",	// Òåêñò êíîïêè "Êóïèòü"
		"MESS_BTN_ADD_TO_BASKET" => "Â êîðçèíó",	// Òåêñò êíîïêè "Äîáàâèòü â êîðçèíó"
		"MESS_BTN_COMPARE" => "Ñðàâíåíèå",	// Òåêñò êíîïêè "Ñðàâíåíèå"
		"MESS_BTN_DETAIL" => "Ïîäðîáíåå",	// Òåêñò êíîïêè "Ïîäðîáíåå"
		"MESS_NOT_AVAILABLE" => "Íåò â íàëè÷èè",	// Ñîîáùåíèå îá îòñóòñòâèè òîâàðà
		"DETAIL_USE_VOTE_RATING" => "N",	// Âêëþ÷èòü ðåéòèíã òîâàðà
		"DETAIL_USE_COMMENTS" => "N",	// Âêëþ÷èòü îòçûâû î òîâàðå
		"DETAIL_BRAND_USE" => "N",	// Èñïîëüçîâàòü êîìïîíåíò "Áðåíäû"
		"USE_FILTER" => "N",	// Ïîêàçûâàòü ôèëüòð
		"ACTION_VARIABLE" => "action",	// Íàçâàíèå ïåðåìåííîé, â êîòîðîé ïåðåäàåòñÿ äåéñòâèå
		"PRODUCT_ID_VARIABLE" => "id",	// Íàçâàíèå ïåðåìåííîé, â êîòîðîé ïåðåäàåòñÿ êîä òîâàðà äëÿ ïîêóïêè
		"USE_COMPARE" => "N",	// Ðàçðåøèòü ñðàâíåíèå òîâàðîâ
		"PRICE_CODE" => "",	// Òèï öåíû
		"USE_PRICE_COUNT" => "N",	// Èñïîëüçîâàòü âûâîä öåí ñ äèàïàçîíàìè
		"SHOW_PRICE_COUNT" => "1",	// Âûâîäèòü öåíû äëÿ êîëè÷åñòâà
		"PRICE_VAT_INCLUDE" => "Y",	// Âêëþ÷àòü ÍÄÑ â öåíó
		"PRICE_VAT_SHOW_VALUE" => "N",	// Îòîáðàæàòü çíà÷åíèå ÍÄÑ
		"BASKET_URL" => "/personal/basket.php",	// URL, âåäóùèé íà ñòðàíèöó ñ êîðçèíîé ïîêóïàòåëÿ
		"USE_PRODUCT_QUANTITY" => "N",	// Ðàçðåøèòü óêàçàíèå êîëè÷åñòâà òîâàðà
		"ADD_PROPERTIES_TO_BASKET" => "Y",	// Äîáàâëÿòü â êîðçèíó ñâîéñòâà òîâàðîâ è ïðåäëîæåíèé
		"PRODUCT_PROPS_VARIABLE" => "prop",	// Íàçâàíèå ïåðåìåííîé, â êîòîðîé ïåðåäàþòñÿ õàðàêòåðèñòèêè òîâàðà
		"PARTIAL_PRODUCT_PROPERTIES" => "N",	// Ðàçðåøèòü äîáàâëÿòü â êîðçèíó òîâàðû, ó êîòîðûõ çàïîëíåíû íå âñå õàðàêòåðèñòèêè
		"PRODUCT_PROPERTIES" => "",	// Õàðàêòåðèñòèêè òîâàðà, äîáàâëÿåìûå â êîðçèíó
		"SHOW_TOP_ELEMENTS" => "N",	// Âûâîäèòü òîï ýëåìåíòîâ
		"TOP_ELEMENT_COUNT" => "9",
		"TOP_LINE_ELEMENT_COUNT" => "3",
		"TOP_ELEMENT_SORT_FIELD" => "sort",
		"TOP_ELEMENT_SORT_ORDER" => "asc",
		"TOP_ELEMENT_SORT_FIELD2" => "id",
		"TOP_ELEMENT_SORT_ORDER2" => "desc",
		"TOP_PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"TOP_VIEW_MODE" => "SECTION",
		"SECTION_COUNT_ELEMENTS" => "N",	// Ïîêàçûâàòü êîëè÷åñòâî ýëåìåíòîâ â ðàçäåëå
		"SECTION_TOP_DEPTH" => "2",	// Ìàêñèìàëüíàÿ îòîáðàæàåìàÿ ãëóáèíà ðàçäåëîâ
		"SECTIONS_VIEW_MODE" => "LINE",	// Âèä ñïèñêà ïîäðàçäåëîâ
		"SECTIONS_SHOW_PARENT_NAME" => "N",	// Ïîêàçûâàòü íàçâàíèå ðàçäåëà
		"PAGE_ELEMENT_COUNT" => "30",	// Êîëè÷åñòâî ýëåìåíòîâ íà ñòðàíèöå
		"LINE_ELEMENT_COUNT" => "3",	// Êîëè÷åñòâî ýëåìåíòîâ, âûâîäèìûõ â îäíîé ñòðîêå òàáëèöû
		"ELEMENT_SORT_FIELD" => "sort",	// Ïî êàêîìó ïîëþ ñîðòèðóåì òîâàðû â ðàçäåëå
		"ELEMENT_SORT_ORDER" => "asc",	// Ïîðÿäîê ñîðòèðîâêè òîâàðîâ â ðàçäåëå
		"ELEMENT_SORT_FIELD2" => "id",	// Ïîëå äëÿ âòîðîé ñîðòèðîâêè òîâàðîâ â ðàçäåëå
		"ELEMENT_SORT_ORDER2" => "desc",	// Ïîðÿäîê âòîðîé ñîðòèðîâêè òîâàðîâ â ðàçäåëå
		"LIST_PROPERTY_CODE" => array(	// Ñâîéñòâà
			0 => "",
			1 => "",
		),
		"INCLUDE_SUBSECTIONS" => "N",	// Ïîêàçûâàòü ýëåìåíòû ïîäðàçäåëîâ ðàçäåëà
		"LIST_META_KEYWORDS" => "-",	// Óñòàíîâèòü êëþ÷åâûå ñëîâà ñòðàíèöû èç ñâîéñòâà ðàçäåëà
		"LIST_META_DESCRIPTION" => "-",	// Óñòàíîâèòü îïèñàíèå ñòðàíèöû èç ñâîéñòâà ðàçäåëà
		"LIST_BROWSER_TITLE" => "-",	// Óñòàíîâèòü çàãîëîâîê îêíà áðàóçåðà èç ñâîéñòâà ðàçäåëà
		"DETAIL_PROPERTY_CODE" => array(	// Ñâîéñòâà
			0 => "",
			1 => "",
		),
		"DETAIL_META_KEYWORDS" => "-",	// Óñòàíîâèòü êëþ÷åâûå ñëîâà ñòðàíèöû èç ñâîéñòâà
		"DETAIL_META_DESCRIPTION" => "-",	// Óñòàíîâèòü îïèñàíèå ñòðàíèöû èç ñâîéñòâà
		"DETAIL_BROWSER_TITLE" => "-",	// Óñòàíîâèòü çàãîëîâîê îêíà áðàóçåðà èç ñâîéñòâà
		"SECTION_ID_VARIABLE" => "SECTION_ID",	// Íàçâàíèå ïåðåìåííîé, â êîòîðîé ïåðåäàåòñÿ êîä ãðóïïû
		"DETAIL_CHECK_SECTION_ID_VARIABLE" => "N",	// Èñïîëüçîâàòü êîä ãðóïïû èç ïåðåìåííîé, åñëè íå çàäàí ðàçäåë ýëåìåíòà
		"DETAIL_DISPLAY_NAME" => "N",	// Âûâîäèòü íàçâàíèå ýëåìåíòà
		"DETAIL_DETAIL_PICTURE_MODE" => "IMG",	// Ðåæèì ïîêàçà äåòàëüíîé êàðòèíêè
		"DETAIL_ADD_DETAIL_TO_SLIDER" => "N",	// Äîáàâëÿòü äåòàëüíóþ êàðòèíêó â ñëàéäåð
		"DETAIL_DISPLAY_PREVIEW_TEXT_MODE" => "E",	// Ïîêàç îïèñàíèÿ äëÿ àíîíñà íà äåòàëüíîé ñòðàíèöå
		"LINK_IBLOCK_TYPE" => "",	// Òèï èíôîáëîêà, ýëåìåíòû êîòîðîãî ñâÿçàíû ñ òåêóùèì ýëåìåíòîì
		"LINK_IBLOCK_ID" => "",	// ID èíôîáëîêà, ýëåìåíòû êîòîðîãî ñâÿçàíû ñ òåêóùèì ýëåìåíòîì
		"LINK_PROPERTY_SID" => "",	// Ñâîéñòâî, â êîòîðîì õðàíèòñÿ ñâÿçü
		"LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",	// URL íà ñòðàíèöó, ãäå áóäåò ïîêàçàí ñïèñîê ñâÿçàííûõ ýëåìåíòîâ
		"USE_STORE" => "N",	// Ïîêàçûâàòü áëîê "Êîëè÷åñòâî òîâàðà íà ñêëàäå"
		"PAGER_TEMPLATE" => ".default",	// Øàáëîí ïîñòðàíè÷íîé íàâèãàöèè
		"DISPLAY_TOP_PAGER" => "N",	// Âûâîäèòü íàä ñïèñêîì
		"DISPLAY_BOTTOM_PAGER" => "N",	// Âûâîäèòü ïîä ñïèñêîì
		"PAGER_TITLE" => "Òîâàðû",	// Íàçâàíèå êàòåãîðèé
		"PAGER_SHOW_ALWAYS" => "N",	// Âûâîäèòü âñåãäà
		"PAGER_DESC_NUMBERING" => "N",	// Èñïîëüçîâàòü îáðàòíóþ íàâèãàöèþ
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Âðåìÿ êåøèðîâàíèÿ ñòðàíèö äëÿ îáðàòíîé íàâèãàöèè
		"PAGER_SHOW_ALL" => "N",	// Ïîêàçûâàòü ññûëêó "Âñå"
		"AJAX_OPTION_ADDITIONAL" => "",	// Äîïîëíèòåëüíûé èäåíòèôèêàòîð
		"FILTER_VIEW_MODE" => "VERTICAL",	// Âèä îòîáðàæåíèÿ óìíîãî ôèëüòðà
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",	// Íàçâàíèå ïåðåìåííîé, â êîòîðîé ïåðåäàåòñÿ êîëè÷åñòâî òîâàðà
		"COMPONENT_TEMPLATE" => "for_menu",
		"USE_MAIN_ELEMENT_SECTION" => "N",	// Èñïîëüçîâàòü îñíîâíîé ðàçäåë äëÿ ïîêàçà ýëåìåíòà
		"SET_LAST_MODIFIED" => "N",	// Óñòàíàâëèâàòü â çàãîëîâêàõ îòâåòà âðåìÿ ìîäèôèêàöèè ñòðàíèöû
		"SECTION_BACKGROUND_IMAGE" => "-",	// Óñòàíîâèòü ôîíîâóþ êàðòèíêó äëÿ øàáëîíà èç ñâîéñòâà
		"DETAIL_SET_CANONICAL_URL" => "N",	// Óñòàíàâëèâàòü êàíîíè÷åñêèé URL
		"DETAIL_BACKGROUND_IMAGE" => "-",	// Óñòàíîâèòü ôîíîâóþ êàðòèíêó äëÿ øàáëîíà èç ñâîéñòâà
		"SHOW_DEACTIVATED" => "N",	// Ïîêàçûâàòü äåàêòèâèðîâàííûå òîâàðû
		"PAGER_BASE_LINK_ENABLE" => "N",	// Âêëþ÷èòü îáðàáîòêó ññûëîê
		"SHOW_404" => "N",	// Ïîêàç ñïåöèàëüíîé ñòðàíèöû
		"MESSAGE_404" => "",	// Ñîîáùåíèå äëÿ ïîêàçà (ïî óìîë÷àíèþ èç êîìïîíåíòà)
		"SEF_URL_TEMPLATES" => array(
			"sections" => "",
			"section" => "#SECTION_CODE#/",
			"element" => "#SECTION_CODE#/#ELEMENT_CODE#/",
			"compare" => "compare.php?action=#ACTION_CODE#",
			"smart_filter" => "#SECTION_CODE#/filter/#SMART_FILTER_PATH#/apply/",
		),
		"VARIABLE_ALIASES" => array(
			"compare" => array(
				"ACTION_CODE" => "action",
			),
		)
	),
	false
);?>
<?else:?>
<?$APPLICATION->IncludeComponent(
	"bitrix:menu",
	"left",
	array(
		"ROOT_MENU_TYPE" => "left",
		"MAX_LEVEL" => "1",
		"CHILD_MENU_TYPE" => "left",
		"USE_EXT" => "Y",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "N",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array(
		)
	),
	false
);?>
<?endif;?>
				</nav>

			</div>
			<div class="content-side">
					<h1><?$APPLICATION->ShowTitle(false)?></h1><div <?if(CSite::InDir('/services/') or CSite::InDir('/team/')):?> class="" <?else:?> class="content-text" <?endif;?>>
