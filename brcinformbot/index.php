<?php

header('Content-Type: text/html; charset=utf-8');
// подрубаем API
require_once("vendor/autoload.php");

// создаем переменную бота
$token = "380628306:AAE9SvpfmOq2nJCAEJk00Ww4Ghdp71KqnIc";
$bot = new \TelegramBot\Api\Client($token);

// если бот еще не зарегистрирован - регистрируем
if(!file_exists("registered.trigger")){ 
	
	 
	// URl текущей страницы
	$page_url = "https://".$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
	$result = $bot->setWebhook($page_url);
	if($result){
		file_put_contents("registered.trigger",time()); // создаем файл дабы прекратить повторные регистрации
	}
}

// обязательное. Запуск бота
$bot->command('start', function ($message) use ($bot) {
    $answer = 'Добро пожаловать!';
    $bot->sendMessage($message->getChat()->getId(), $answer);
});

// помощ
$bot->command('help', function ($message) use ($bot) {
    $answer = 'Команды:
/help - помощь';
    $bot->sendMessage($message->getChat()->getId(), $answer);
});

$bot->command('whois', function ($message) use ($bot) {
    $text = $message->getText();
    $mess_part = substr($text, 7);
    
    $url = 'http://web.brconsult.pro:88/api/employee/' . $mess_part; 
    // Обращение к Web-сервису; результаты возвращаются в формате XML
    $url = iconv('UTF-8', 'CP1251', $url);
    $response = file_get_contents($url);
    //$response = new SimpleXMLElement($response); 
    // Преобразование XML в объект SimpleXML 
    $answer = $response;
    
    $bot->sendMessage($message->getChat()->getId(), $answer);
});

$bot->command('getpic', function ($message) use ($bot) {
	$pic = "http://aftamat4ik.ru/wp-content/uploads/2017/03/photo_2016-12-13_23-21-07.jpg";

    $bot->sendPhoto($message->getChat()->getId(), $pic);
});

// запускаем обработку
$bot->run();


   
